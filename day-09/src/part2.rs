use std::{ops::Range, slice::Iter};

use itertools::{Diff, Itertools, TupleWindows};
use nom::{
    character::{
        complete::{digit1, multispace1, newline, space1},
        streaming::multispace0,
    },
    combinator::{map_res, recognize, opt},
    multi::{many1, separated_list1},
    IResult, sequence::preceded, bytes::complete::tag,
};
use tracing::info;

use crate::custom_error::AocError;

fn parse_input(input: &str) -> IResult<&str, Vec<Vec<i64>>> {

    separated_list1(
        newline,
        separated_list1(
            space1,
            map_res(recognize(preceded(opt(tag("-")), digit1)), |digit_str: &str| digit_str.parse::<i64>()),
        ),
    )(input)
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (input, lines) = parse_input(input).expect("Could not parse input");

    info!("{:#?}", lines);
    let res: i64 = lines
        .iter()
        .map(|val| process_line(val.clone()))
        .map(|x| x.1)
        .sum();

    Ok(res.to_string())
}

pub fn process_line(input: Vec<i64>) -> (Vec<i64>, i64) {
    let input = input.clone();
    let it: TupleWindows<Iter<i64>, (&i64, &i64)> = input.iter().tuple_windows();

    let diff: Vec<i64> = it.map(|(a, b)| b - a).collect();

    if diff.iter().all(|v| v.eq(&0)) {
        info!("done");
        (input.clone(), *input.last().expect("no value"))
    } else {
        let res = process_line(diff.clone());
        (
            input.clone(),
            input.first().expect("should be present") - res.1,
        )
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        let input = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";
        assert_eq!("2", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    //#[case(vec![-6, -3, 0, 3, 6, 9, 12, 15], (vec![-6, -3, 0, 3, 6, 9, 12, 15], 18))]
    #[case(vec![0, 3, 6, 9, 12, 15], (vec![0, 3, 6, 9, 12, 15], -3))]
    #[case(vec![1, 3, 6, 10, 15, 21], (vec![1, 3, 6, 10, 15, 21], 0))]
    #[case(vec![10, 13, 16, 21, 30, 45], (vec![10, 13, 16, 21, 30, 45], 5))]
    fn test_hands(#[case] input: Vec<i64>, #[case] expected: (Vec<i64>, i64)) {
        assert_eq!(process_line(input), expected);
    }

    #[test_log::test(rstest)]
    #[case("0 3 6 9 12 15
1 3 6 10 15 21", vec![vec![0, 3, 6, 9, 12, 15],vec![1, 3, 6, 10, 15, 21]])]
    #[case("-5 -9 -13 -17 -21", vec![vec![-5, -9, -13, -17, -21]])]
    #[case("1 3 6 10 15 21", vec![vec![1, 3, 6, 10, 15, 21]])]
    #[case("10 13 16 21 30 45", vec![vec![10, 13, 16, 21, 30, 45]])]
    #[case("0 3 6 9 12 15", vec![vec![0, 3, 6, 9, 12, 15]])]
    fn test_input(#[case] input: &str, #[case] expected: Vec<Vec<i64>>) {
        let expected = Ok(("", expected));
        assert_eq!(parse_input(input), expected);
    }
}
