use std::{str::FromStr, usize};

use glam::IVec2;
use itertools::Itertools;
use nom::{
    bytes::complete::{tag, take_until, take_while_m_n},
    character::complete::{digit1, multispace1, newline, one_of},
    combinator::map_res,
    multi::separated_list1,
    sequence::{delimited, pair, preceded, tuple},
    IResult,
};
use tracing::info;

use crate::custom_error::AocError;

// expected: `"952408144115"`,
// actual: `"952404941483"`'
// diff 3_202_632

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (_, lines) = parse_input(input).expect("Could not parse input");
    info!("{:?}", lines);

    let start_pos: IVec2 = IVec2::new(0, 0);
    let path: Vec<IVec2> = vec![start_pos];

    let (pos, path) = lines.iter().fold((start_pos, path), |mut acc, ele| {
        info!("{:?}", ele);
        (0..ele.dig_length).for_each(|_| {
            acc.0 = acc.0 + ele.direction.ivec2();
        });
        acc.1.push(acc.0.clone());

        acc
    });

    let row_min = path
        .iter()
        .map(|node| node.y)
        .min()
        .expect("There should be a path");

    let col_min = path
        .iter()
        .map(|node| node.x)
        .min()
        .expect("There should be a path");

    //let shift = IVec2::new(col_min.abs(), row_min.abs());

    //let path: Vec<IVec2> = path.iter().map(|old_pos| *old_pos + shift).collect();

    let rows = path
        .iter()
        .map(|node| node.y)
        .max()
        .expect("There should be a path");
    let cols = path
        .iter()
        .map(|node| node.x)
        .max()
        .expect("There should be a path");

    let res = calculate_volume(&path);

    //info!("res: {:?}", path);
    info!("bounds cols: {col_min} - {cols}");
    info!("bounds rows: {row_min} - {rows}");
    info!("path-len: {:?}", path.len());

    //info!("{}\n", as_string(rows, cols, &path));
    //info!("{}", as_string_filled(rows, cols, &path));

    Ok(res.to_string())
}

fn calculate_volume(path: &Vec<IVec2>) -> isize {
    let sum_left: isize = path
        .iter()
        .circular_tuple_windows()
        .map(|(a1, a2)| a1.x as isize * a2.y as isize)
        .sum();
    let sum_right: isize = path
        .iter()
        .circular_tuple_windows()
        .map(|(a1, a2)| a1.y as isize * a2.x as isize)
        .sum();

    let circ = circumference(path) as isize;
    (sum_left - sum_right) / 2 + circ / 2 + 1
}

fn circumference(path: &Vec<IVec2>) -> usize {
    path.iter()
        .circular_tuple_windows()
        .map(|(a, b)| *a - *b)
        .map(|v| v.x.abs() as usize + v.y.abs() as usize)
        .sum::<usize>()
}

#[derive(Debug, Eq, PartialEq, PartialOrd)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

#[derive(PartialEq, Debug, Eq, PartialOrd, Ord)]
enum DigDirection {
    Up,
    Down,
    Left,
    Right,
}

impl DigDirection {
    fn ivec2(&self) -> IVec2 {
        match &self {
            DigDirection::Up => IVec2::new(0, -1),
            DigDirection::Down => IVec2::new(0, 1),
            DigDirection::Left => IVec2::new(-1, 0),
            DigDirection::Right => IVec2::new(1, 0),
        }
    }
}

impl TryFrom<char> for DigDirection {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'L' => Ok(DigDirection::Left),
            'R' => Ok(DigDirection::Right),
            'U' => Ok(DigDirection::Up),
            'D' => Ok(DigDirection::Down),
            '0' => Ok(DigDirection::Right),
            '1' => Ok(DigDirection::Down),
            '2' => Ok(DigDirection::Left),
            '3' => Ok(DigDirection::Up),
            _ => Err(format!("Unknown direction: {}", value)),
        }
    }
}

impl TryFrom<&str> for DigDirection {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        value
            .chars()
            .next()
            .ok_or_else(|| format!("Unknown direction: {}", value))?
            .try_into()
    }
}

fn from_hex(input: &str) -> Result<u32, std::num::ParseIntError> {
    u32::from_str_radix(input, 16)
}

fn is_hex_digit(c: char) -> bool {
    c.is_digit(16)
}

fn hex_distance(input: &str) -> IResult<&str, u32> {
    map_res(take_while_m_n(5, 5, is_hex_digit), from_hex)(input)
}

fn hex_direction(input: &str) -> IResult<&str, DigDirection> {
    map_res(take_while_m_n(1, 1, is_hex_digit), DigDirection::try_from)(input)
}

fn hex_dig(input: &str) -> IResult<&str, InputLine> {
    let (input, (dig_length, direction)) = delimited(
        tag("("),
        preceded(tag("#"), pair(hex_distance, hex_direction)),
        tag(")"),
    )(input)?;

    Ok((
        input,
        InputLine {
            direction,
            dig_length: dig_length as i32,
        },
    ))
}

fn parse_line(input: &str) -> IResult<&str, InputLine> {
    let (input, (_, line)) = tuple((take_until("("), hex_dig))(input)?;

    Ok((input, line))
}

#[derive(PartialEq, Debug, PartialOrd, Eq)]
pub struct InputLine {
    direction: DigDirection,
    dig_length: i32,
}

pub fn parse_input(input: &str) -> IResult<&str, Vec<InputLine>> {
    let (input, lines) = separated_list1(newline, parse_line)(input)?;
    Ok((input, lines))
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    //#[test]
    fn test_process() -> miette::Result<()> {
        todo!("haven't built test yet");
        let input = "";
        assert_eq!("", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"R 6 (#70c710)
    D 5 (#0dc571)
    L 2 (#5713f0)
    D 2 (#d2c081)
    R 2 (#59c680)
    D 2 (#411b91)
    L 5 (#8ceee2)
    U 2 (#caa173)
    L 1 (#1b58a2)
    U 2 (#caa171)
    R 2 (#7807d2)
    U 3 (#a77fa3)
    L 2 (#015232)
    U 2 (#7a21e3)"},
        "952408144115"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(vec![IVec2::new(0,0)],0)]
    #[case(vec![
        IVec2::new(2,7),
        IVec2::new(10,1),
        IVec2::new(8,6),
        IVec2::new(11,7),
        IVec2::new(7,10),
        ],32)]
    #[case(vec![
            IVec2::new(1,6),
            IVec2::new(3,1),
            IVec2::new(7,2),
            IVec2::new(4,4),
            IVec2::new(8,5),
        ],16)]
    fn test_calc_volume(#[case] input: Vec<IVec2>, #[case] expected: isize) -> miette::Result<()> {
        assert_eq!(expected, calculate_volume(&input));
        Ok(())
    }
}
