use std::str::FromStr;

use glam::IVec2;
use itertools::Itertools;
use nom::{
    bytes::complete::{tag, take_while_m_n},
    character::complete::{digit1, multispace1, newline, one_of},
    combinator::map_res,
    multi::separated_list1,
    sequence::{delimited, tuple},
    IResult,
};
use tracing::info;

use crate::custom_error::AocError;

// 52871 too low
// 114132 too high
// 108909

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (_, lines) = parse_input(input).expect("Could not parse input");
    info!("{:?}", lines);

    let path: Vec<IVec2> = to_path2(&lines);

    let row_min = path
        .iter()
        .map(|node| node.y)
        .min()
        .expect("There should be a path");

    let col_min = path
        .iter()
        .map(|node| node.x)
        .min()
        .expect("There should be a path");

    let shift = IVec2::new(col_min.abs(), row_min.abs());

    let path: Vec<IVec2> = path.iter().map(|old_pos| *old_pos + shift).collect();

    let rows = path
        .iter()
        .map(|node| node.y)
        .max()
        .expect("There should be a path");
    let cols = path
        .iter()
        .map(|node| node.x)
        .max()
        .expect("There should be a path");

    info!("res: {:?}", path);
    info!("bounds cols: {col_min} - {cols}");
    info!("bounds rows: {row_min} - {rows}");
    info!("path-len: {:?}", path.len());

    info!("{}\n", as_string(rows, cols, &path));
    info!("{}", as_string_filled(rows, cols, &path));

    //Ok(calculate_volume(rows, cols, &path).to_string())
    Ok((calculate_volume2(&path)).to_string())
}

fn to_path(lines: &Vec<InputLine>) -> Vec<IVec2> {
    let start_pos: IVec2 = IVec2::new(0, 0);
    let mut path: Vec<IVec2> = Vec::new();
    let (pos, path) = lines.iter().fold((start_pos, path), |mut acc, ele| {
        (0..ele.dig_length).for_each(|_| {
            acc.0 = acc.0 + ele.direction.ivec2();
            acc.1.push(acc.0.clone());
        });
        acc
    });
    path
}

fn circumference(path: &Vec<IVec2>) -> usize {
    path.iter()
        .circular_tuple_windows()
        .map(|(a, b)| *a - *b)
        .map(|v| v.x.abs() as usize + v.y.abs() as usize)
        .sum()
}

fn to_path2(lines: &Vec<InputLine>) -> Vec<IVec2> {
    let start_pos: IVec2 = IVec2::new(0, 0);
    let path: Vec<IVec2> = Vec::new();
    let circ = 0;
    let (pos, path, circ) = lines.iter().fold((start_pos, path, circ), |mut acc, ele| {
        info!("{:?}", ele);
        (0..ele.dig_length).for_each(|_| {
            acc.0 = acc.0 + ele.direction.ivec2();
        });
        acc.1.push(acc.0.clone());

        acc
    });
    path
}

fn as_string(rows: i32, cols: i32, path: &Vec<IVec2>) -> String {
    let mut s = String::from("\n");
    for y in 0..=rows {
        for x in 0..=cols {
            if path.contains(&IVec2::new(x, y)) {
                s.push_str("#")
            } else {
                s.push_str(".")
            }
        }
        s.push_str("\n")
    }
    s
}

fn as_string_filled(rows: i32, cols: i32, path: &Vec<IVec2>) -> String {
    let mut s = String::from("\n");
    for y in 0..=rows {
        let mut crossings = 0;
        for x in 0..=cols {
            let current: IVec2 = IVec2::new(x, y);

            if path.contains(&current) {
                let contains_before = path.contains(&(current + IVec2::new(-1, 0)));
                let contains_after = path.contains(&(current + IVec2::new(1, 0)));
                let contains_above = path.contains(&(current + IVec2::new(0, -1)));
                let contains_bellow = path.contains(&(current + IVec2::new(0, 1)));

                if contains_before && contains_after {
                    s.push_str("-");
                } else if contains_before && contains_above {
                    crossings += 1;
                    s.push_str("J");
                } else if contains_before && contains_bellow {
                    s.push_str("7");
                } else if contains_after && contains_above {
                    s.push_str("L");
                    crossings += 1;
                } else if contains_after && contains_bellow {
                    s.push_str("F");
                } else if !contains_after && !contains_before {
                    crossings += 1;
                    s.push_str("#");
                }
            } else {
                if crossings % 2 != 0 {
                    s.push_str("#");
                } else {
                    s.push_str(".")
                }
            }
        }
        s.push_str("\n")
    }
    s
}

fn calculate_volume2(path: &Vec<IVec2>) -> isize {
    let sum_left: isize = path
        .iter()
        .circular_tuple_windows()
        .map(|(a1, a2)| a1.x as isize * a2.y as isize)
        .sum();
    let sum_right: isize = path
        .iter()
        .circular_tuple_windows()
        .map(|(a1, a2)| a1.y as isize * a2.x as isize)
        .sum();

    let circumference: usize = circumference(path);

    (sum_left - sum_right + circumference as isize) / 2
}

fn calculate_volume(rows: i32, cols: i32, path: &Vec<IVec2>) -> i32 {
    let mut count = 0;

    for y in 0..=rows {
        let mut crossings = 0;
        let mut row_count = 0;
        for x in 0..=cols {
            let current: IVec2 = IVec2::new(x, y);
            if path.contains(&current) {
                let contains_before = path.contains(&(current + IVec2::new(-1, 0)));
                let contains_after = path.contains(&(current + IVec2::new(1, 0)));
                let contains_above = path.contains(&(current + IVec2::new(0, -1)));
                let contains_bellow = path.contains(&(current + IVec2::new(0, 1)));

                if contains_before && contains_after {
                } else if contains_before && contains_above {
                    crossings += 1;
                } else if contains_after && contains_above {
                    crossings += 1;
                } else if !contains_after && !contains_before {
                    crossings += 1;
                }
            } else {
                if crossings % 2 != 0 {
                    row_count += 1;
                }
            }
        }
        count = count + row_count;
    }
    count + path.len() as i32
}
#[derive(Debug, Eq, PartialEq, PartialOrd)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

#[derive(PartialEq, Debug, Eq, PartialOrd, Ord)]
enum DigDirection {
    Up,
    Down,
    Left,
    Right,
}

impl DigDirection {
    fn ivec2(&self) -> IVec2 {
        match &self {
            DigDirection::Up => IVec2::new(0, -1),
            DigDirection::Down => IVec2::new(0, 1),
            DigDirection::Left => IVec2::new(-1, 0),
            DigDirection::Right => IVec2::new(1, 0),
        }
    }
}

impl TryFrom<char> for DigDirection {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'L' => Ok(DigDirection::Left),
            'R' => Ok(DigDirection::Right),
            'U' => Ok(DigDirection::Up),
            'D' => Ok(DigDirection::Down),
            _ => Err(format!("Unknown direction: {}", value)),
        }
    }
}

fn from_hex(input: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(input, 16)
}

fn is_hex_digit(c: char) -> bool {
    c.is_digit(16)
}

fn hex_primary(input: &str) -> IResult<&str, u8> {
    map_res(take_while_m_n(2, 2, is_hex_digit), from_hex)(input)
}

fn hex_color(input: &str) -> IResult<&str, Color> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue)) = tuple((hex_primary, hex_primary, hex_primary))(input)?;

    Ok((input, Color { red, green, blue }))
}

fn parse_line(input: &str) -> IResult<&str, InputLine> {
    let (input, (direction, _, dig_length, _, color)) = tuple((
        map_res(one_of("LRUD"), DigDirection::try_from),
        multispace1,
        map_res(digit1, FromStr::from_str),
        multispace1,
        delimited(tag("("), hex_color, tag(")")),
    ))(input)?;

    Ok((
        input,
        InputLine {
            direction,
            dig_length,
            color,
        },
    ))
}

#[derive(PartialEq, Debug, PartialOrd, Eq)]
pub struct InputLine {
    direction: DigDirection,
    dig_length: i32,
    color: Color,
}

pub fn parse_input(input: &str) -> IResult<&str, Vec<InputLine>> {
    let (input, lines) = separated_list1(newline, parse_line)(input)?;
    Ok((input, lines))
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    //#[test]
    fn test_process() -> miette::Result<()> {
        todo!("haven't built test yet");
        let input = "";
        assert_eq!("", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"R 6 (#70c710)
    D 5 (#0dc571)
    L 2 (#5713f0)
    D 2 (#d2c081)
    R 2 (#59c680)
    D 2 (#411b91)
    L 5 (#8ceee2)
    U 2 (#caa173)
    L 1 (#1b58a2)
    U 2 (#caa171)
    R 2 (#7807d2)
    U 3 (#a77fa3)
    L 2 (#015232)
    U 2 (#7a21e3)"},
        "62"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
