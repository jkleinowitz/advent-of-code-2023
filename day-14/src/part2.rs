use std::{
    collections::{hash_map::DefaultHasher, HashMap, HashSet},
    f64::consts::E,
    hash::{self, Hash, Hasher},
    iter::zip,
};

use cached::{proc_macro::cached, SizedCache};
use grid::Grid;
use itertools::Itertools;
use tracing::{debug, info};
use tracing_subscriber::field::debug;

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let mut dish = Dish::parse(input);

    info!("initial: \n{}", dish.out());
    let end = 1000;
    let mut count = 0;

    let mut loads = Vec::new();

    let initial_load = dish.calculate_load();

    let (grid, load) = (0..end).fold((dish.grid, initial_load), |(grid, load), _| {
        if loads.contains(&load) {
            info!("found cycle at: {}", count);

            // 98471 high

            let cycle_len = loads.len();
            //let remaining = end - count;
            let remaining = end % cycle_len;
            info!("remaining: {}, cycle_len: {}", remaining, cycle_len);
            info!("loads: {:?}", loads);
        } else {
            loads.push(load);
        }
        if count % 700 == 0 {
            info!("Load: \n{}", load);
            info!("count: {}", count);
        }
        count += 1;
        let (grid, new_load) = cycle_and_load(grid);
        (grid, new_load)
    });

    //info!("cycled: \n{}", dish.out());
    //let load = dish.calculate_load();

    Ok(load.to_string())
}

struct Dish {
    grid: Grid<char>,
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
enum Direction {
    North,
    South,
    East,
    West,
}

#[cached]
fn lanes_cached(grid: Grid<char>, direction: Direction) -> Vec<Vec<char>> {
    let grid = grid;
    match direction {
        Direction::North => grid
            .iter_cols()
            .map(|lane| lane.copied().collect_vec())
            .collect_vec(),
        Direction::South => grid
            .iter_cols()
            .map(|lane| lane.rev().copied().collect_vec())
            .collect(),
        Direction::East => grid
            .iter_rows()
            .map(|lane| lane.rev().copied().collect_vec())
            .collect(),
        Direction::West => grid
            .iter_rows()
            .map(|lane| lane.copied().collect_vec())
            .collect(),
    }
}

#[cached]
fn tilt_itern_cached(lanes: Vec<Vec<char>>) -> Vec<Vec<char>> {
    lanes
        .into_iter()
        //.enumerate()
        .map(|lane| {
            //debug!("lane: {:?}", lane);
            let mut cube_rocke_indices: Vec<usize> = lane
                .iter()
                .enumerate()
                .filter(|(_, &c)| c == '#')
                .map(|(i, _)| i)
                .collect_vec();
            cube_rocke_indices.insert(0, 0);
            cube_rocke_indices.push(lane.len());

            let cube_rock_indices = cube_rocke_indices.iter().tuple_windows().collect_vec();

            //debug!("cube_rock_indices: {:?}", cube_rock_indices);

            //let mut new_lane: Vec<char> = vec![];

            cube_rock_indices
                .into_iter()
                .map(|(start, end)| {
                    lane[*start..*end]
                        .to_vec()
                        .into_iter()
                        .sorted_unstable_by(|a, b| match (a, b) {
                            ('#', _) => std::cmp::Ordering::Less,
                            ('O', '.') => std::cmp::Ordering::Less,
                            ('.', '0') => std::cmp::Ordering::Greater,
                            _ => std::cmp::Ordering::Equal,
                        })
                })
                .flatten()
                .collect_vec()
        })
        .collect_vec()
}

#[cached]
fn grid_for_tilt_cached(
    direction: Direction,
    new_lanes: Vec<Vec<char>>,
    cols: usize,
) -> Grid<char> {
    match direction {
        Direction::North => {
            let new_lanes = new_lanes.into_iter().flatten().collect_vec();

            Grid::from_vec_with_order(new_lanes, cols, grid::Order::ColumnMajor)
        }
        Direction::South => {
            let new_lanes: Vec<char> = new_lanes
                .into_iter()
                .map(|l| l.into_iter().rev())
                .flatten()
                .collect_vec();

            Grid::from_vec_with_order(new_lanes, cols, grid::Order::ColumnMajor)
        }
        Direction::East => {
            let new_lanes: Vec<char> = new_lanes
                .into_iter()
                .map(|l| l.into_iter().rev())
                .flatten()
                .collect_vec();
            Grid::from_vec_with_order(new_lanes, cols, grid::Order::RowMajor)
        }
        Direction::West => {
            let new_lanes = new_lanes.into_iter().flatten().collect_vec();
            Grid::from_vec_with_order(new_lanes, cols, grid::Order::RowMajor)
        }
    }
}

fn calculate_load(grid: &Grid<char>) -> usize {
    let num_rows = grid.rows();
    grid.iter_rows()
        .enumerate()
        .map(|(i, row)| row.filter(|&c| c == &'O').count() * (num_rows - i))
        .sum::<usize>()
}

#[cached(
    type = "SizedCache<usize, Grid<char>>",
    create = "{ SizedCache::with_size(100) }",
    convert = r#"{ calculate_load(&grid) }"#
)]
fn cycle_hashed(grid: Grid<char>) -> Grid<char> {
    let cols = grid.cols();

    let grid = tilt_cached(grid, Direction::North, cols);
    let grid = tilt_cached(grid, Direction::West, cols);
    let grid = tilt_cached(grid, Direction::South, cols);
    let grid = tilt_cached(grid, Direction::East, cols);
    grid
}

#[cached]
fn cycle_and_load(grid: Grid<char>) -> (Grid<char>, usize) {
    let cols = grid.cols();

    let grid = tilt_cached(grid, Direction::North, cols);
    let grid = tilt_cached(grid, Direction::West, cols);
    let grid = tilt_cached(grid, Direction::South, cols);
    let grid = tilt_cached(grid, Direction::East, cols);
    let load = calculate_load(&grid);
    (grid, load)
}
impl Dish {
    fn parse(input: &str) -> Self {
        let chars: Vec<char> = input
            .lines()
            .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
            .collect();
        let cols = input.lines().next().unwrap().chars().count();
        let grid = Grid::from_vec(chars, cols);
        Self { grid }
    }

    fn calculate_load(&self) -> usize {
        let num_rows = self.grid.rows();
        self.grid
            .iter_rows()
            .enumerate()
            .map(|(i, row)| row.filter(|&c| c == &'O').count() * (num_rows - i))
            .sum::<usize>()
    }
    fn out(&self) -> String {
        self.grid
            .iter_rows()
            .map(|row| row.collect::<String>())
            .join("\n")
    }

    fn cycle(&mut self) {
        self.grid = cycle_hashed(self.grid.to_owned());
    }

    fn tilt(&self, direction: Direction) -> Grid<char> {
        /*
        let mut hasher = DefaultHasher::new();
        self.grid.hash(&mut hasher);
        hasher.finish();*/

        let cols = self.grid.cols();
        tilt_cached(self.grid.to_owned(), direction, cols)
    }

    fn grid_for_tilt(&self, direction: &Direction, new_lanes: Vec<Vec<char>>) -> Grid<char> {
        grid_for_tilt_cached(*direction, new_lanes, self.grid.cols())
    }

    fn tilt_intern(&self, lanes: Vec<Vec<char>>) -> Vec<Vec<char>> {
        tilt_itern_cached(lanes)
    }

    fn lanes_for_tilt(&self, direction: &Direction) -> Vec<Vec<char>> {
        lanes_cached(self.grid.to_owned(), *direction)
    }
}

fn tilt_cached(to_owned: Grid<char>, direction: Direction, cols: usize) -> Grid<char> {
    let lanes = lanes_cached(to_owned, direction);
    let new_lanes = tilt_itern_cached(lanes);
    let new_lanes: Vec<Vec<char>> = new_lanes
        .into_iter()
        .map(|l| l.into_iter().collect())
        .collect();
    grid_for_tilt_cached(direction, new_lanes, cols)
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use rstest::rstest;

    #[test_log::test(test)]
    fn test_process() -> miette::Result<()> {
        let input = indoc! {"O....#....
        O.OO#....#
        .....##...
        OO.#O....O
        .O.....O#.
        O.#..O.#.#
        ..O..#O..O
        .......O..
        #....###..
        #OO..#...."};
        assert_eq!("64", process(input)?);
        Ok(())
    }

    /*
    #[test_log::test(rstest)]
    #[case(indoc! {"O....#....
    O.OO#....#
    .....##...
    OO.#O....O
    .O.....O#.
    O.#..O.#.#
    ..O..#O..O
    .......O..
    #....###..
    #OO..#...."},
        "64"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
     */
}
