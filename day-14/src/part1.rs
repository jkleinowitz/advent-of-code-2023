use std::iter::zip;

use grid::Grid;
use itertools::Itertools;
use tracing::{debug, info};
use tracing_subscriber::field::debug;

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let mut dish = Dish::parse(input);

    info!("initial: \n{}", dish.out());
    dish.grid = dish.tilt_north();
    info!("tilted: \n{}", dish.out());
    let load = dish.calculate_load();

    Ok(load.to_string())
}

struct Dish {
    grid: Grid<char>,
}

impl Dish {
    fn parse(input: &str) -> Self {
        let chars: Vec<char> = input
            .lines()
            .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
            .collect();
        let cols = input.lines().next().unwrap().chars().count();
        let grid = Grid::from_vec(chars, cols);
        Self { grid }
    }

    fn new(grid: Grid<char>) -> Self {
        Self { grid }
    }
    fn calculate_load(&self) -> usize {
        let num_rows = self.grid.rows();
        self.grid
            .iter_rows()
            .enumerate()
            .map(|(i, row)| row.filter(|&c| c == &'O').count() * (num_rows - i))
            .sum::<usize>()
    }
    fn out(&self) -> String {
        self.grid
            .iter_rows()
            .map(|row| row.collect::<String>())
            .join("\n")
    }
    fn tilt_north(&mut self) -> Grid<char> {
        let lanes = self
            .grid
            .iter_cols()
            .map(|row| row.collect_vec())
            .collect_vec();

        let new_lanes = lanes
            .iter()
            .enumerate()
            .map(|(i, lane)| {
                debug!("lane: {:?}", lane);
                let mut cube_rocke_indices: Vec<usize> = lane
                    .iter()
                    .enumerate()
                    .filter(|(i, &c)| c == &'#')
                    .map(|(i, c)| i)
                    .collect_vec();
                cube_rocke_indices.insert(0, 0);
                cube_rocke_indices.push(lane.len());

                let cube_rock_indices = cube_rocke_indices.iter().tuple_windows().collect_vec();

                //debug!("cube_rock_indices: {:?}", cube_rock_indices);

                let mut new_row: Vec<char> = vec![];
                for (start, end) in cube_rock_indices {
                    let mut slice: Vec<&char> = lane[*start..*end].to_vec();
                    info!("slice (unsorted): {:?}", slice);

                    slice.sort_unstable_by(|a, b| match (a, b) {
                        ('#', _) => std::cmp::Ordering::Less,
                        ('O', '.') => std::cmp::Ordering::Less,
                        ('.', '0') => std::cmp::Ordering::Greater,
                        _ => std::cmp::Ordering::Equal,
                    });
                    info!("slice (sorted): {:?}", slice);
                    new_row.extend(slice.iter().cloned());
                }

                let old = lane.iter().join("");
                let new: String = new_row.iter().join("");

                info!("\nold ({}): {}\nnew ({}): {}", i, old, i, new);
                new_row
            })
            .collect_vec();

        let mut new_grid = Grid::from_vec(
            new_lanes.into_iter().flatten().collect_vec(),
            self.grid.cols(),
        );
        new_grid.transpose();
        new_grid
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use rstest::rstest;

    #[test_log::test(test)]
    fn test_process() -> miette::Result<()> {
        let input = indoc! {"O....#....
        O.OO#....#
        .....##...
        OO.#O....O
        .O.....O#.
        O.#..O.#.#
        ..O..#O..O
        .......O..
        #....###..
        #OO..#...."};
        assert_eq!("136", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"O....#....
    O.OO#....#
    .....##...
    OO.#O....O
    .O.....O#.
    O.#..O.#.#
    ..O..#O..O
    .......O..
    #....###..
    #OO..#...."},
        "136"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
