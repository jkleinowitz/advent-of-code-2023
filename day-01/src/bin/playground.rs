use nom::{IResult, combinator::opt, bytes::complete::tag};


fn abcd_opt(i: &[u8]) -> IResult<&[u8], Option<&[u8]>> {
  opt(tag("abcd"))(i)
}


fn str_opt(input : &str) -> IResult<&str, Option<&str>> {
  opt(tag("abcd"))(input)
}


fn main() {

    assert_eq!(str_opt("abcdxxx"), Ok(("xxx", Some("abcd"))));
    //assert_eq!(abcd_opt(&b"xxabcdxxx"[..]), Ok((&b"xxx"[..], Some(&b"abcd"[..]))));
    //assert_eq!(abcd_opt(&b"efghxxx"[..]), Ok((&b"efghxxx"[..], None)));
    //dbg!(output);
}


