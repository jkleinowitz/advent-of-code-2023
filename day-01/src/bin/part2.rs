use nom::branch::alt;
use nom::bytes::complete::{take_while_m_n, take, take_while1};
use nom::character::complete::{digit0, i32, one_of};
use nom::character::{is_alphanumeric, is_digit};
use nom::combinator::{not, opt, value, peek};
use nom::{bytes::complete::tag, multi::many0};
use nom::{combinator::map_res, IResult};
use nom::{AsChar, Slice};

fn main() {
    let input = include_str!("./input1.txt");
    let output = part2(input);
    dbg!(output);
}

fn is_num_string(input: &str) -> IResult<&str, &str> {
    alt((
        tag("one"),
        tag("two"),
        tag("three"),
        tag("four"),
        tag("five"),
        tag("six"),
        tag("seven"),
        tag("eight"),
        tag("9"),
        take_while_m_n(1, 1, AsChar::is_dec_digit),
    ))(input)
}

fn parse_num_string(input: &str) -> IResult<&str, i32> {
    alt((
        value(1, tag("one")),
        value(2, tag("two")),
        value(3, tag("three")),
        value(4, tag("four")),
        value(5, tag("five")),
        value(6, tag("six")),
        value(7, tag("seven")),
        value(8, tag("eight")),
        value(9, tag("nine")),
    ))(input)
}

fn convert3(input: &str) -> IResult<&str, i32> {
    alt((parse_num_string, i32))(input)
}

fn convert(input: &str) -> IResult<&str, Vec<i32>> {
    many0(alt((parse_num_string, i32)))(input)
}

fn convert2(input: &str) -> IResult<&str, Vec<i32>> {
    let mut data = input.clone();

    let it = std::iter::from_fn(|| match parse_num_string(input) {
        Ok((i, o)) => {
            data = i;
            Some(o)
        }
        Err(_) => None,
    });

    let num_parser = many0(alt((parse_num_string, i32)));

    alt((num_parser, value(vec![], tag(" "))))(input)
}

fn part2(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let mut res: String = String::new();
            let mut i = line;
            while i.len() > 0 {
                let pr = convert3(i);
                if let Ok((i2, r)) = pr {
                    dbg!(i2);
                    res.push_str(&r.to_string());
                }
                i = &i[1..];
            }

            let t: Vec<_> = res.chars().filter(|c| c.is_digit(10)).collect();
            let res: String = [t.first().unwrap(), t.last().unwrap()]
                .into_iter()
                .collect();
            res.parse::<i32>().unwrap()
        })
        .sum()
}


fn parse_str_num(i: &str) -> IResult<&str, &str> {    
    let a: IResult<&str, &str> = alt((
        value("1", tag("one")),
        value("2", tag("two")),
        value("3", tag("three")),
        value("4", tag("four")),
        value("5", tag("five")),
        value("6", tag("six")),
        value("7", tag("seven")),
        value("8", tag("eight")),
        value("9", tag("nine")),
    ))(i);
    println!("a: {:?},", a);
    a
    /*

    
    
    let (tail, out) = peek(alt((
        value("1", tag("one")),
        value("2", tag("two")),
        value("3", tag("three")),
        value("4", tag("four")),
        value("5", tag("five")),
        value("6", tag("six")),
        value("7", tag("seven")),
        value("8", tag("eight")),
        value("9", tag("nine")),
    )))(i)?;
    println!("tail: {}, out: {}", tail, out);
    let (tail, _) = take(1usize)(tail)?;
    Ok((tail, out))*/
    
}

fn parse_num(i: &str) -> IResult<&str, Vec<&str>> {
    
    many0(alt((
        take_while1(move |c: char| c.is_numeric()),
        parse_str_num,
        value("", take(1usize)),
    )))(i)
}

fn parser(i: &str) -> i32 {
    println!("{}", i);
    let (_, num) = parse_num(i).expect("numer not returned");
    let binding = num.join("");
    let mut chars = binding.trim().chars();
    println!("{:?}", chars);
    let first = chars.next().unwrap_or('0');
    let last = chars.next_back().unwrap_or(first);
    let val = format!("{}{}", first, last).parse::<i32>().unwrap();
    println!("{}", val);
    println!("-----------------");
    val
}

#[cfg(test)]
mod tests {
    use crate::{convert, is_num_string, part2, parser};

    #[test]
    fn test_peek() {
        let r = parser("eighteighthree");
        println!("{}", r);

    }

    #[test]
    fn test1() {
        let result = part2(
            "two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen",
        );
        assert_eq!(result, 281);
    }

    #[test]
    fn test2() {
        let result = convert("two1nine");
        let v = result.unwrap().1;

        assert_eq!(v, vec![2, 1, 9]);
    }
    #[test]
    fn test3() {
        let result = convert("eightwothree");
        let v = result.unwrap().1;

        assert_eq!(v, vec![8, 3]);
    }
}
