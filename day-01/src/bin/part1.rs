use std::process::Output;

fn main() {
    let input = include_str!("./input1.txt");
    let output = part1(input);
    dbg!(output);
}

fn part1(input: &str) -> i32 {
    input.lines()
    .map(|line| {
        let t: Vec<_> = line.chars()
        .filter(|c| c.is_digit(10))
        .collect();
        let res: String = [t.first().unwrap(), t.last().unwrap()].into_iter().collect();
        res.parse::<i32>().unwrap()
        }).sum()
}

#[cfg(test)]
mod tests {
    use crate::part1;

    #[test]
    fn test1() {
        let result = part1("1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet");
        assert_eq!(result, 142);
    }
}