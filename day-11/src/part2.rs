use std::fmt;

use grid::Grid;
use itertools::Itertools;
use tracing::{info, debug};

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grid = to_array(input);

    info!("{}", print(&grid));
    //let grid = enlarge(grid);


    let rows_to_duplicate: Vec<usize> = grid
        .iter_rows()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();
    let cols_to_duplicate: Vec<usize> = grid
        .iter_cols()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();
    

    info!("{}", print(&grid));
    info!("Rows to enlarge: {:?}", rows_to_duplicate);
    info!("Cols to enlarge: {:?}", cols_to_duplicate);

    let galaxy_indices: Vec<(usize, (usize, usize))> = grid
        .indexed_iter()
        .filter_map(|(i, ele)| {
            if *ele == MapElement::Galaxy {
                Some(i)
            } else {
                None
            }
        })
        .enumerate()
        .collect();

    info!("{:?}", galaxy_indices);


    let combinations: Vec<_> = galaxy_indices.iter().combinations(2).collect();
    info!("# Combinations: {:?}", combinations.len());

    let empty_multiplier: i64 = 1000000;

    let res = combinations.iter().map(|v| {
        let (galaxy_a, a) = v.get(0).expect("Should containt two values");
        let (galaxy_b, b) = v.get(1).expect("Should containt two values");

        

        let range_x = if a.1 < b.1 {
            a.1..=b.1
        } else {
            b.1..=a.1
        };
        let range_y = if a.0 < b.0 {
            a.0..=b.0
        } else {
            b.0..=a.0
        };

        let x: i64 = range_x.map(|col| match cols_to_duplicate.contains(&col) {
            true => empty_multiplier,
            false => 1,
        }).sum();
        let y: i64 = range_y.map(|row| match rows_to_duplicate.contains(&row) {
            true => empty_multiplier,
            false => 1,
        }).sum();

        let dist = x + y -2;

        debug!("{} {:?} -> {} {:?}: {} + {} = {}", galaxy_a+1, a, galaxy_b+1, b, y, x,dist);

        dist
    }).collect_vec();
    debug!("Found {} distances", res.len());
    let res: i64 = res.iter().sum();
    let res: usize = res.try_into().expect("Could not convert");
    Ok(res.to_string())
}

#[derive(PartialEq, PartialOrd, Clone)]
enum MapElement {
    Galaxy,
    Empty,
}

impl From<char> for MapElement {
    fn from(c: char) -> Self {
        match c {
            '#' => MapElement::Galaxy,
            '.' => MapElement::Empty,
            _ => panic!("Invalid character for MapElement: {}", c),
        }
    }
}

impl fmt::Display for MapElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MapElement::Galaxy => write!(f, "#"),
            MapElement::Empty => write!(f, "."),
        }
    }
}

fn combinations(galaxy_indices: &Vec<(usize, usize)>) -> Vec<Vec<&(usize, usize)>> {
    galaxy_indices.iter().combinations(2).collect()
}


fn enlarge(mut grid: Grid<MapElement>) -> Grid<MapElement> {
    let rows_to_duplicate: Vec<usize> = grid
        .iter_rows()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();
    let cols_to_duplicate: Vec<usize> = grid
        .iter_cols()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();

    let multiplier = 10;

    let number_rows = grid.rows();
    let number_cols = grid.cols() + (rows_to_duplicate.len() * multiplier);
    rows_to_duplicate
        .iter()
        .rev()
        .for_each(|i| {
            (0..multiplier).for_each(|_| {
                grid.insert_row(*i, vec![MapElement::Empty; number_rows])
            });
        });
    cols_to_duplicate
        .iter()
        .rev()
        .for_each(|i| {
            (0..multiplier).for_each(|_| {
                grid.insert_col(*i, vec![MapElement::Empty; number_cols]);
            });
        }); 
    grid
}

fn print(grid: &Grid<MapElement>) -> String {
    let mut s = String::from("");
    let mut current_row = 0;
    grid.indexed_iter().for_each(|e| {
        let index = e.0;
        if current_row != index.0 {
            s.push_str("\n");
            current_row = current_row + 1
        }
        s.push_str(&e.1.to_string());
    });
    s
}

fn to_array(input: &str) -> Grid<MapElement> {
    let chars: Vec<MapElement> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .map(|c| MapElement::from(c))
        .collect();
    let cols = input.lines().next().unwrap().chars().count();
    let grid = Grid::from_vec(chars, cols);
    grid
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use tracing::debug;

    use super::*;

    #[test_log::test(rstest)]
    fn test_process() -> miette::Result<()> {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        assert_eq!("8410", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case(
        "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....",
        "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#......."
    )]
    fn test_compare_line(#[case] input: &str, #[case] expected: &str) {
        let grid = to_array(input);

        //debug!("{}", print(&grid));
        //let grid = enlarge(grid);
        //debug!("{}", print(&grid));

        //let expected = Ok(("", expected));

        //assert_eq!(expected, print(&grid))
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case(
        vec![(0,1), (0,2), (0,3)],
        vec![vec![&(0,1), &(0,2)],vec![&(0,1), &(0,3)],vec![&(0,2), &(0,3)]]   
    )]
    fn test_combinations(#[case] input: Vec<(usize, usize)>, #[case] expected: Vec<Vec<&(usize, usize)>>) {
        assert_eq!(expected, combinations(&input))

    }
}
