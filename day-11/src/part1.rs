use std::fmt;

use grid::Grid;
use itertools::Itertools;
use tracing::info;

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grid = to_array(input);

    info!("{}", print(&grid));
    let grid = enlarge(grid);
    info!("{}", print(&grid));

    let galaxy_indices: Vec<(usize, usize)> = grid
        .indexed_iter()
        .filter_map(|(i, ele)| {
            if *ele == MapElement::Galaxy {
                Some(i)
            } else {
                None
            }
        })
        .collect();

    let combinations: Vec<_> = galaxy_indices.iter().combinations(2).collect();
    info!("{:?}", combinations);

    let res: usize =     combinations.iter().map(|v| {
        let a = v.get(0).expect("Should containt two values");
        let b = v.get(1).expect("Should containt two values");
        a.0.abs_diff(b.0) + 
        a.1.abs_diff(b.1)
    }).sum();
    Ok(res.to_string())
    //todo!("day 01 - part 1");
}

#[derive(PartialEq, PartialOrd, Clone)]
enum MapElement {
    Galaxy,
    Empty,
}

impl From<char> for MapElement {
    fn from(c: char) -> Self {
        match c {
            '#' => MapElement::Galaxy,
            '.' => MapElement::Empty,
            _ => panic!("Invalid character for MapElement: {}", c),
        }
    }
}

impl fmt::Display for MapElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MapElement::Galaxy => write!(f, "#"),
            MapElement::Empty => write!(f, "."),
        }
    }
}

fn combinations(galaxy_indices: &Vec<(usize, usize)>) -> Vec<Vec<&(usize, usize)>> {
    galaxy_indices.iter().combinations(2).collect()
}
fn enlarge(mut grid: Grid<MapElement>) -> Grid<MapElement> {
    let rows_to_duplicate: Vec<usize> = grid
        .iter_rows()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();
    let cols_to_duplicate: Vec<usize> = grid
        .iter_cols()
        .enumerate()
        .filter_map(|(i, row)| {
            row.into_iter()
                .all(|x| x == &MapElement::Empty)
                .then_some(i)
        })
        .collect();

    let number_rows = grid.rows();
    let number_cols = grid.cols() + rows_to_duplicate.len();
    rows_to_duplicate
        .iter()
        .rev()
        .for_each(|i| grid.insert_row(*i, vec![MapElement::Empty; number_rows]));
    cols_to_duplicate
        .iter()
        .rev()
        .for_each(|i| grid.insert_col(*i, vec![MapElement::Empty; number_cols]));
    grid
}

fn print(grid: &Grid<MapElement>) -> String {
    let mut s = String::from("");
    let mut current_row = 0;
    grid.indexed_iter().for_each(|e| {
        let index = e.0;
        if current_row != index.0 {
            s.push_str("\n");
            current_row = current_row + 1
        }
        s.push_str(&e.1.to_string());
    });
    s
}

fn to_array(input: &str) -> Grid<MapElement> {
    let chars: Vec<MapElement> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .map(|c| MapElement::from(c))
        .collect();
    let cols = input.lines().next().unwrap().chars().count();
    let grid = Grid::from_vec(chars, cols);
    grid
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use tracing::debug;

    use super::*;

    #[test_log::test(rstest)]
    fn test_process() -> miette::Result<()> {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";
        assert_eq!("374", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case(
        "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....",
        "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#......."
    )]
    fn test_compare_line(#[case] input: &str, #[case] expected: &str) {
        let grid = to_array(input);

        debug!("{}", print(&grid));
        let grid = enlarge(grid);
        debug!("{}", print(&grid));

        //let expected = Ok(("", expected));

        assert_eq!(expected, print(&grid))
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case(
        vec![(0,1), (0,2), (0,3)],
        vec![vec![&(0,1), &(0,2)],vec![&(0,1), &(0,3)],vec![&(0,2), &(0,3)]]   
    )]
    fn test_combinations(#[case] input: Vec<(usize, usize)>, #[case] expected: Vec<Vec<&(usize, usize)>>) {
        assert_eq!(expected, combinations(&input))

    }
}
