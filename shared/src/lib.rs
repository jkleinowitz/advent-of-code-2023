use std::fmt;

pub struct Rectangle {
    pub left_upper: Point<i32>,
    pub right_lower: Point<i32>
}

impl Rectangle {
    pub fn overlaps(&self, other: &Rectangle) -> bool {
        let x_overlap = self.left_upper.x <= other.right_lower.x && self.right_lower.x >= other.left_upper.x;
        let y_overlap = self.left_upper.y <= other.right_lower.y && self.right_lower.y >= other.left_upper.y;
        x_overlap && y_overlap
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Point<N> {
    pub x: N,
    pub y: N,
}

#[derive(Debug)]
pub struct PartNumber {
    pub number: i32,
    pub pos: Point<i32>
}

impl PartNumber {
    pub fn width(&self) -> usize {
        self.number.abs().to_string().len()
    }
    pub fn end(&self) -> Point<i32> {
        Point {
            x: self.pos.x + self.width() as i32,
            y: self.pos.y,
        }
    }
}

impl fmt::Display for PartNumber {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "('{}', {})", self.number, self.pos)
    }
}

impl<N: std::fmt::Debug> std::fmt::Display for Point<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({:?}, {:?})", self.x, self.y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
