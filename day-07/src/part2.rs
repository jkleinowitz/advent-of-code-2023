use std::{cmp::Ordering, collections::HashMap, iter::zip, ops::Mul, str::FromStr};

use crate::custom_error::AocError;
use itertools::Itertools;
use nom::{
    character::complete::{alphanumeric1, digit1, multispace1, newline},
    combinator::map_res,
    error::{Error, ErrorKind},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};
use tracing::{debug, info};

#[derive(PartialEq, Debug, Eq, Hash, PartialOrd, Ord)]
enum CamelCard {
    J,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    Q,
    K,
    A,
}

impl FromStr for CamelCard {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(CamelCard::A),
            "K" => Ok(CamelCard::K),
            "Q" => Ok(CamelCard::Q),
            "J" => Ok(CamelCard::J),
            "T" => Ok(CamelCard::T),
            "9" => Ok(CamelCard::Nine),
            "8" => Ok(CamelCard::Eight),
            "7" => Ok(CamelCard::Seven),
            "6" => Ok(CamelCard::Six),
            "5" => Ok(CamelCard::Five),
            "4" => Ok(CamelCard::Four),
            "3" => Ok(CamelCard::Three),
            "2" => Ok(CamelCard::Two),
            _ => Err(()),
        }
    }
}

#[derive(PartialEq, Debug, Eq)]
pub struct InputLine {
    cards: Vec<CamelCard>,
    bid: i64,
}

#[derive(PartialEq, Debug, Eq, PartialOrd, Ord)]
pub enum Hand {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl InputLine {
    fn hand(&self) -> Hand {
        let mut occ = self.cards.iter().fold(HashMap::new(), |mut acc, c| {
            *acc.entry(c).or_insert(0) += 1;
            acc
        });

        let nr_jokers = occ.remove(&CamelCard::J).unwrap_or(0);

        let occ_values: Vec<i64> = occ.into_values().collect();

        if nr_jokers >= 4
            || nr_jokers == 3 && occ_values.iter().any(|count| *count == 2)
            || nr_jokers == 2 && occ_values.iter().any(|count| *count == 3)
            || nr_jokers == 1 && occ_values.iter().any(|count| *count == 4)
        {
            Hand::FiveOfAKind
        } else if nr_jokers == 3
            || nr_jokers == 2 && occ_values.iter().any(|count| *count == 2)
            || nr_jokers == 1 && occ_values.iter().any(|count| *count == 3)
        {
            Hand::FourOfAKind
        } else if nr_jokers == 1 && occ_values.iter().filter(|&&count| count == 2).count() == 2 {
            Hand::FullHouse
        } else if nr_jokers == 2 || nr_jokers == 1 && occ_values.iter().any(|count| *count == 2) {
            Hand::ThreeOfAKind
        } else if nr_jokers == 1 {
            Hand::OnePair
        } else if nr_jokers == 0 {
            if occ_values.iter().any(|count| *count == 5) {
                Hand::FiveOfAKind
            } else if occ_values.iter().any(|count| *count == 4) {
                Hand::FourOfAKind
            } else if occ_values.iter().any(|count| *count == 3)
                && occ_values.iter().any(|count| *count == 2)
            {
                Hand::FullHouse
            } else if occ_values.iter().any(|&count| count == 3) {
                Hand::ThreeOfAKind
            } else if occ_values.iter().filter(|&&count| count == 2).count() == 2 {
                Hand::TwoPair
            } else if occ_values.iter().any(|&count| count == 2) {
                Hand::OnePair
            } else {
                Hand::HighCard
            }
        } else {
            panic!("Unexpected")
        }
    }
}

impl PartialOrd for InputLine {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for InputLine {
    fn cmp(&self, other: &Self) -> Ordering {
        debug!(
            "{:?} vs {:?}: {:?}",
            self.hand(),
            other.hand(),
            self.hand().cmp(&other.hand())
        );
        if self.hand() == other.hand() {
            let iter = zip(&self.cards, &other.cards);
            let r = iter
                .inspect(|(a, b)| debug!("{:?} vs {:?}: {:?}", a, b, a.cmp(b)))
                .map(|(a, b)| a.cmp(b))
                .find(|o| !o.is_eq());

            return r.unwrap_or(Ordering::Equal);
        }
        self.hand().cmp(&other.hand())
    }
}

fn parse_line(input: &str) -> IResult<&str, InputLine> {
    let (input, (hand, bid)) = separated_pair(
        alphanumeric1,
        multispace1,
        map_res(digit1, FromStr::from_str),
    )(input)?;

    let (_, hand) = parse_hand(hand)?;
    Ok((input, InputLine { cards: hand, bid }))
}

fn parse_hand(input: &str) -> IResult<&str, Vec<CamelCard>> {
    let cards: Vec<CamelCard> = input
        .split_inclusive(|_| true)
        .flat_map(CamelCard::from_str)
        .collect();

    if cards.len() != input.len() {
        Err(nom::Err::Error(Error {
            input: input,
            code: ErrorKind::Fail,
        }))
    } else {
        Ok(("", cards))
    }
}

pub fn parse_input(input: &str) -> IResult<&str, Vec<InputLine>> {
    let (input, lines) = separated_list1(newline, parse_line)(input)?;
    Ok((input, lines))
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let line_count = input.lines().count();
    let (_, lines) = parse_input(input).expect("Parsing failed");

    let sorted: Vec<&InputLine> = lines.iter().sorted().collect();
    assert_eq!(line_count, sorted.len());

    for ele in sorted {
        info!("{:?}, {:?}", ele, ele.hand());
    }

    let res: usize = lines
        .iter()
        .sorted()
        .enumerate()
        .inspect(|(i, line)| info!(i, ": {:?}", line.hand()))
        .map(|(i, line)| (i + 1).mul(line.bid as usize))
        .sum();

    Ok(res.to_string())
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        let input = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";
        assert_eq!("5905", process(input)?);
        Ok(())
    }

    #[test_log::test]
    fn test_process2() -> miette::Result<()> {
        let input = "2345A 1
Q2KJJ 13
Q2Q2Q 19
T3T3J 17
T3Q33 11
2345J 3
J345A 2
32T3K 5
T55J5 29
KK677 7
KTJJT 34
QQQJA 31
JJJJJ 37
JAAAA 43
AAAAJ 59
AAAAA 61
2AAAA 23
2JJJJ 53
JJJJ2 41";
        assert_eq!("6839", process(input)?);
        Ok(())
    }

    #[test]
    fn test_process_hand() -> miette::Result<()> {
        let input = "32T3K";
        assert_eq!(
            Ok((
                "",
                vec![
                    CamelCard::Three,
                    CamelCard::Two,
                    CamelCard::T,
                    CamelCard::Three,
                    CamelCard::K
                ]
            )),
            parse_hand(input)
        );
        Ok(())
    }

    #[test]
    fn test_parse_line() -> miette::Result<()> {
        let input = "32T3K 765";
        assert_eq!(
            Ok((
                "",
                InputLine {
                    cards: vec![
                        CamelCard::Three,
                        CamelCard::Two,
                        CamelCard::T,
                        CamelCard::Three,
                        CamelCard::K
                    ],
                    bid: 765
                }
            )),
            parse_line(input)
        );
        Ok(())
    }
    #[test]
    fn test_parse_lines() -> miette::Result<()> {
        let input = "32T3K 765
T55J5 684";
        assert_eq!(
            Ok((
                "",
                vec![
                    InputLine {
                        cards: vec![
                            CamelCard::Three,
                            CamelCard::Two,
                            CamelCard::T,
                            CamelCard::Three,
                            CamelCard::K
                        ],
                        bid: 765
                    },
                    InputLine {
                        cards: vec![
                            CamelCard::T,
                            CamelCard::Five,
                            CamelCard::Five,
                            CamelCard::J,
                            CamelCard::Five
                        ],
                        bid: 684
                    }
                ]
            )),
            parse_input(input)
        );
        Ok(())
    }

    #[rstest]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FourOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::T,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FullHouse)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FourOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::T,
            CamelCard::Three,
            CamelCard::Five,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::TwoPair)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::T,
            CamelCard::Three,
            CamelCard::K,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::OnePair)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::A,
            CamelCard::Three,
            CamelCard::K,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::HighCard)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FourOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FiveOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::Five,
            CamelCard::Three,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }, Hand::FourOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::Five,
            CamelCard::Three,
            CamelCard::J,
            CamelCard::Two
        ],
        bid: 684
    }, Hand::ThreeOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::Five,
            CamelCard::Three,
            CamelCard::Two,
            CamelCard::Two
        ],
        bid: 684
    }, Hand::ThreeOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::J,
            CamelCard::J,
            CamelCard::Two,
            CamelCard::Two
        ],
        bid: 684
    }, Hand::FiveOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::J,
            CamelCard::J,
            CamelCard::J,
            CamelCard::Two
        ],
        bid: 684
    }, Hand::FiveOfAKind)]
    fn test_hands(#[case] input: InputLine, #[case] expected: Hand) {
        assert_eq!(input.hand(), expected);
    }

    #[test]
    fn test_compare() {
        assert_eq!(CamelCard::A.cmp(&CamelCard::Eight), Ordering::Greater);
        assert_eq!(CamelCard::Q.cmp(&CamelCard::T), Ordering::Greater);
        assert_eq!(CamelCard::T.cmp(&CamelCard::K), Ordering::Less);
        assert_eq!(CamelCard::J.cmp(&CamelCard::Two), Ordering::Less);
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case((InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }),  Some(Ordering::Equal))]
    #[case((InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::K,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    }),  Some(Ordering::Less))]
    #[case((InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::Five,
            CamelCard::Five,
            CamelCard::J,
            CamelCard::Five
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::K,
            CamelCard::T,
            CamelCard::J,
            CamelCard::J,
            CamelCard::T
        ],
        bid: 684
    }),  Some(Ordering::Less))]
    fn test_compare_line(#[case] a: (InputLine, InputLine), #[case] expected: Option<Ordering>) {
        assert_eq!(a.0.partial_cmp(&a.1), expected)
    }

    #[test]
    fn test_sort_cards() {
        let mut input = vec![
            CamelCard::Eight,
            CamelCard::Q,
            CamelCard::Seven,
            CamelCard::K,
            CamelCard::Six,
            CamelCard::A,
            CamelCard::Two,
            CamelCard::J,
            CamelCard::T,
            CamelCard::Nine,
            CamelCard::Five,
            CamelCard::Four,
            CamelCard::Three,
        ];
        input.sort();

        let mut expected = vec![
            CamelCard::A,
            CamelCard::K,
            CamelCard::Q,
            CamelCard::T,
            CamelCard::Nine,
            CamelCard::Eight,
            CamelCard::Seven,
            CamelCard::Six,
            CamelCard::Five,
            CamelCard::Four,
            CamelCard::Three,
            CamelCard::Two,
            CamelCard::J,
        ];
        expected.reverse();

        assert_eq!(input, expected);
    }
}
