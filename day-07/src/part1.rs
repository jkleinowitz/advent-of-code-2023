use std::{
    cmp::Ordering,
    collections::HashMap,
    iter::zip,
    ops::Mul,
    str::FromStr, i64,
};

use crate::custom_error::AocError;
use itertools::Itertools;
use nom::{
    character::complete::{alphanumeric1, digit1, multispace1, newline},
    combinator::map_res,
    error::{Error, ErrorKind},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};
use tracing::info;

#[derive(PartialEq, Debug, Eq, Hash, PartialOrd, Ord)]
enum CamelCard {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    T,
    J,
    Q,
    K,
    A,
}

impl FromStr for CamelCard {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(CamelCard::A),
            "K" => Ok(CamelCard::K),
            "Q" => Ok(CamelCard::Q),
            "J" => Ok(CamelCard::J),
            "T" => Ok(CamelCard::T),
            "9" => Ok(CamelCard::NINE),
            "8" => Ok(CamelCard::EIGHT),
            "7" => Ok(CamelCard::SEVEN),
            "6" => Ok(CamelCard::SIX),
            "5" => Ok(CamelCard::FIVE),
            "4" => Ok(CamelCard::FOUR),
            "3" => Ok(CamelCard::THREE),
            "2" => Ok(CamelCard::TWO),
            _ => Err(()),
        }
    }
}

#[derive(PartialEq, Debug, Ord, Eq)]
pub struct InputLine {
    cards: Vec<CamelCard>,
    bid: i64,
}

#[derive(PartialEq, Debug, Eq, PartialOrd, Ord)]
pub enum Hand {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl InputLine {
    fn hand(&self) -> Hand {
        let occ = self.cards.iter().fold(HashMap::new(), |mut acc, c| {
            *acc.entry(c).or_insert(0) += 1;
            acc
        });

        let occ_values: Vec<i64> = occ.into_values().collect();
        if occ_values.iter().any(|count| *count == 5) {
            Hand::FiveOfAKind
        } else if occ_values.iter().any(|count| *count == 4) {
            Hand::FourOfAKind
        } else if occ_values.iter().any(|count| *count == 3)
            && occ_values.iter().any(|count| *count == 2)
        {
            Hand::FullHouse
        } else if occ_values.iter().any(|count| *count == 3) {
            Hand::ThreeOfAKind
        } else if occ_values.iter().filter(|&count| *count == 2).count() == 2 {
            Hand::TwoPair
        } else if occ_values.iter().any(|count| *count == 2) {
            Hand::OnePair
        } else {
            Hand::HighCard
        }
    }
}

impl PartialOrd for InputLine {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        info!("{:?} vs {:?}: {:?}", self.hand(), other.hand(), self.hand().cmp(&other.hand()));
        if self.hand() == other.hand() {
            let iter = zip(&self.cards, &other.cards);
            let r = iter
                .inspect(|(a, b)| info!("{:?} vs {:?}: {:?}", a, b, a.cmp(b)))
                .map(|(a, b)| a.cmp(b))
                .find(|o| !o.is_eq());

            return r.or(Some(Ordering::Equal));
        }
        Some(self.hand().cmp(&other.hand()))
    }
}

fn parse_line(input: &str) -> IResult<&str, InputLine> {
    let (input, (hand, bid)) = separated_pair(
        alphanumeric1,
        multispace1,
        map_res(digit1, FromStr::from_str),
    )(input)?;

    let (_, hand) = parse_hand(hand)?;
    Ok((input, InputLine { cards: hand, bid }))
}

fn parse_hand(input: &str) -> IResult<&str, Vec<CamelCard>> {
    //let (input, card_str) = take_while1(is_alphanumeric)(input)?;
    let cards: Vec<CamelCard> = input
        .split_inclusive(|_| true)
        .flat_map(CamelCard::from_str)
        .collect();

    if cards.len() != input.len() {
        Err(nom::Err::Error(Error {
            input: input,
            code: ErrorKind::Fail,
        }))
    } else {
        Ok(("", cards))
    }
}

pub fn parse_input(input: &str) -> IResult<&str, Vec<InputLine>> {
    let (input, lines) = separated_list1(newline, parse_line)(input)?;
    Ok((input, lines))
}

#[tracing::instrument]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (_, lines) = parse_input(input).expect("Parsing failed");

    let res: usize = lines
        .iter()
        .sorted()
        //.rev()
        .enumerate()
        //.inspect(|(i, line)| info!(i, ": {:?}", line.hand()))
        .map(|(i, line)| (i + 1).mul(line.bid as usize))
        .sum();

    Ok(res.to_string())
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        let input = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";
        assert_eq!("6440", process(input)?);
        Ok(())
    }

    #[test]
    fn test_process_input_line() -> miette::Result<()> {
        let input = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";
        assert_eq!("6440", process(input)?);
        Ok(())
    }

    #[test]
    fn test_process_hand() -> miette::Result<()> {
        let input = "32T3K";
        assert_eq!(
            Ok((
                "",
                vec![
                    CamelCard::THREE,
                    CamelCard::TWO,
                    CamelCard::T,
                    CamelCard::THREE,
                    CamelCard::K
                ]
            )),
            parse_hand(input)
        );
        Ok(())
    }

    #[test]
    fn test_parse_line() -> miette::Result<()> {
        let input = "32T3K 765";
        assert_eq!(
            Ok((
                "",
                InputLine {
                    cards: vec![
                        CamelCard::THREE,
                        CamelCard::TWO,
                        CamelCard::T,
                        CamelCard::THREE,
                        CamelCard::K
                    ],
                    bid: 765
                }
            )),
            parse_line(input)
        );
        Ok(())
    }
    #[test]
    fn test_parse_lines() -> miette::Result<()> {
        let input = "32T3K 765
T55J5 684";
        assert_eq!(
            Ok((
                "",
                vec![
                    InputLine {
                        cards: vec![
                            CamelCard::THREE,
                            CamelCard::TWO,
                            CamelCard::T,
                            CamelCard::THREE,
                            CamelCard::K
                        ],
                        bid: 765
                    },
                    InputLine {
                        cards: vec![
                            CamelCard::T,
                            CamelCard::FIVE,
                            CamelCard::FIVE,
                            CamelCard::J,
                            CamelCard::FIVE
                        ],
                        bid: 684
                    }
                ]
            )),
            parse_input(input)
        );
        Ok(())
    }

    #[rstest]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::J,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::ThreeOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::T,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::FullHouse)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::FourOfAKind)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::T,
            CamelCard::THREE,
            CamelCard::FIVE,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::TwoPair)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::T,
            CamelCard::THREE,
            CamelCard::K,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::OnePair)]
    #[case(InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::A,
            CamelCard::THREE,
            CamelCard::K,
            CamelCard::FIVE
        ],
        bid: 684
    }, Hand::HighCard)]
    fn test_hands(#[case] input: InputLine, #[case] expected: Hand) {
        assert_eq!(input.hand(), expected);
    }

    #[test]
    fn test_compare() {
        assert_eq!(CamelCard::A.cmp(&CamelCard::EIGHT), Ordering::Greater);
        assert_eq!(CamelCard::Q.cmp(&CamelCard::T), Ordering::Greater);
        assert_eq!(CamelCard::T.cmp(&CamelCard::K), Ordering::Less);
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case((InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::J,
            CamelCard::FIVE
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::J,
            CamelCard::FIVE
        ],
        bid: 684
    }),  Some(Ordering::Equal))]
    #[case((InputLine {
        cards: vec![
            CamelCard::T,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::J,
            CamelCard::FIVE
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::K,
            CamelCard::FIVE,
            CamelCard::FIVE,
            CamelCard::J,
            CamelCard::FIVE
        ],
        bid: 684
    }),  Some(Ordering::Less))]
    #[case((InputLine {
        cards: vec![
            CamelCard::J,
            CamelCard::K,
            CamelCard::K,
            CamelCard::K,
            CamelCard::TWO
        ],
        bid: 684
    },InputLine {
        cards: vec![
            CamelCard::Q,
            CamelCard::Q,
            CamelCard::Q,
            CamelCard::Q,
            CamelCard::TWO
        ],
        bid: 684
    }),  Some(Ordering::Less))]
    fn test_compare_line(#[case] a: (InputLine, InputLine), #[case] expected: Option<Ordering>) {
        assert_eq!(a.0.partial_cmp(&a.1), expected)
    }

    #[test]
    fn test_sort_cards() {
        let mut input = vec![
            CamelCard::EIGHT,
            CamelCard::Q,
            CamelCard::SEVEN,
            CamelCard::K,
            CamelCard::SIX,
            CamelCard::A,
            CamelCard::TWO,
            CamelCard::J,
            CamelCard::T,
            CamelCard::NINE,
            CamelCard::FIVE,
            CamelCard::FOUR,
            CamelCard::THREE,
        ];
        input.sort();

        let mut expected = vec![
            CamelCard::A,
            CamelCard::K,
            CamelCard::Q,
            CamelCard::J,
            CamelCard::T,
            CamelCard::NINE,
            CamelCard::EIGHT,
            CamelCard::SEVEN,
            CamelCard::SIX,
            CamelCard::FIVE,
            CamelCard::FOUR,
            CamelCard::THREE,
            CamelCard::TWO,
        ];
        expected.reverse();

        assert_eq!(input, expected);
    }
}
