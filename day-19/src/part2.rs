use std::{
    cmp,
    collections::HashMap,
    iter::successors,
    ops::{Range, RangeInclusive},
};

use crate::custom_error::AocError;
use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::{is_not, tag, take_till, take_while1, take_while_m_n},
    character::{
        complete::{alpha1, anychar, char, digit1, line_ending, multispace1, newline, one_of},
        is_alphabetic,
    },
    combinator::{map, map_parser, map_res},
    multi::separated_list1,
    sequence::{delimited, pair, preceded, separated_pair, tuple},
    IResult,
};
use tracing::info;

#[derive(Debug)]
struct Op {
    var: String,
    nr: usize,
    target: RuleResult,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum RuleResult {
    Workflow(String),
    Rejected,
    Accepted,
}

#[derive(Debug)]
enum Rule {
    GreaterThan(Op),
    LowerThan(Op),
    Move(RuleResult),
}

impl Rule {
    fn evaluate(&self, part: &Part) -> Option<RuleResult> {
        match self {
            Rule::GreaterThan(op) => part
                .val_for_attr(op.var.as_str())
                .ok()
                .map(|v| v > op.nr)
                .filter(|result| *result)
                .map(|_| op.target.clone()),
            Rule::LowerThan(op) => part
                .val_for_attr(op.var.as_str())
                .ok()
                .map(|v| v < op.nr)
                .filter(|result| *result)
                .map(|_| op.target.clone()),
            Rule::Move(target) => Some(target.clone()),
        }
    }
}

#[derive(Debug)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl Workflow {
    fn handle(&self, part: &Part) -> Option<RuleResult> {
        self.rules
            .iter()
            .flat_map(|r| r.evaluate(part))
            .take(1)
            .next()
    }
}

#[derive(Debug, Clone)]
pub struct PartRange {
    x: RangeInclusive<usize>,
    m: RangeInclusive<usize>,
    a: RangeInclusive<usize>,
    s: RangeInclusive<usize>,
}

impl PartRange {
    fn range_for_attr(&mut self, attr_name: &str, new_range: RangeInclusive<usize>) {
        match attr_name {
            "a" => self.a = new_range,
            "s" => self.s = new_range,
            "m" => self.m = new_range,
            "x" => self.x = new_range,
            _ => panic!("Err, attribute {} doesn't exist on part", attr_name),
        };
    }

    fn with_modified_range(
        attr_name: &str,
        new_range: RangeInclusive<usize>,
        source: &PartRange,
    ) -> Self {
        let mut pr = source.clone();
        pr.range_for_attr(attr_name, new_range);
        pr
    }
}

#[derive(Debug)]
pub struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

impl Part {
    fn val_for_attr(&self, attr_name: &str) -> Result<usize, String> {
        match attr_name {
            "a" => Ok(self.a),
            "s" => Ok(self.s),
            "m" => Ok(self.m),
            "x" => Ok(self.x),
            _ => Err(format!(
                "Err, attribute {} doesn't exist on part",
                attr_name
            )),
        }
    }
}

fn parse_rule_result(input: &str) -> IResult<&str, RuleResult> {
    let (input, result) = alt((
        map(tag("A"), |_| RuleResult::Accepted),
        map(tag("R"), |_| RuleResult::Rejected),
        map(alpha1, |target: &str| {
            RuleResult::Workflow(target.to_string())
        }),
    ))(input)?;
    Ok((input, result))
}

fn parse_rule(input: &str) -> IResult<&str, Rule> {
    let (input, rule) = alt((
        map(
            tuple((
                alpha1,
                char('<'),
                map_res(digit1, |s: &str| s.parse::<usize>()),
                char(':'),
                parse_rule_result,
            )),
            |(var, _, nr, _, target)| {
                Rule::LowerThan(Op {
                    nr,
                    var: var.to_string(),
                    target: target,
                })
            },
        ),
        map(
            tuple((
                alpha1,
                char('>'),
                map_res(digit1, |s: &str| s.parse::<usize>()),
                char(':'),
                parse_rule_result,
            )),
            |(var, _, nr, _, target)| {
                Rule::GreaterThan(Op {
                    nr,
                    var: var.to_string(),
                    target,
                })
            },
        ),
        map(parse_rule_result, |target| Rule::Move(target)),
    ))(input)?;

    Ok((input, rule))
}
fn parse_workflow(input: &str) -> IResult<&str, Workflow> {
    let (input, (workflow_name, rules)) = pair(
        alpha1,
        delimited(tag("{"), separated_list1(tag(","), parse_rule), tag("}")),
    )(input)?;
    Ok((
        input,
        Workflow {
            name: workflow_name.to_string(),
            rules,
        },
    ))
}

pub fn parse_part(input: &str) -> IResult<&str, Part> {
    let (input, values) = delimited(
        tag("{"),
        separated_list1(
            tag(","),
            alt((
                preceded(
                    tag("x="),
                    map_res(digit1, |digit_str: &str| digit_str.parse::<u32>()),
                ),
                preceded(
                    tag("m="),
                    map_res(digit1, |digit_str: &str| digit_str.parse::<u32>()),
                ),
                preceded(
                    tag("a="),
                    map_res(digit1, |digit_str: &str| digit_str.parse::<u32>()),
                ),
                preceded(
                    tag("s="),
                    map_res(digit1, |digit_str: &str| digit_str.parse::<u32>()),
                ),
            )),
        ),
        tag("}"),
    )(input)?;
    Ok((
        input,
        Part {
            x: values[0] as usize,
            m: values[1] as usize,
            a: values[2] as usize,
            s: values[3] as usize,
        },
    ))
}

fn parse_input(input: &str) -> IResult<&str, (Vec<Workflow>, Vec<Part>)> {
    let (input, lines) = separated_pair(
        separated_list1(newline, parse_workflow),
        multispace1,
        separated_list1(newline, parse_part),
    )(input)?;
    Ok((input, lines))
}

fn find_possible_ranges(
    pr: PartRange,
    wf: &Workflow,
    wf_map: &HashMap<String, Workflow>,
) -> Vec<(PartRange, RuleResult)> {
    let res = wf
        .rules
        .iter()
        .map(|r| match r {
            Rule::GreaterThan(op) => {
                let new_range = op.nr..=4000;
                let pr = PartRange::with_modified_range(op.var.as_str(), new_range, &pr);
                match &op.target {
                    RuleResult::Workflow(wf_name) => {
                        find_possible_ranges(pr, &wf_map[wf_name], wf_map)
                    }
                    RuleResult::Rejected => vec![(pr.clone(), RuleResult::Rejected)],
                    RuleResult::Accepted => vec![(pr.clone(), RuleResult::Accepted)],
                }
            }
            Rule::LowerThan(op) => {
                let new_range = 0..=op.nr;
                let pr = PartRange::with_modified_range(op.var.as_str(), new_range, &pr);
                match &op.target {
                    RuleResult::Workflow(wf_name) => {
                        find_possible_ranges(pr, &wf_map[wf_name], wf_map)
                    }
                    RuleResult::Rejected => vec![(pr.clone(), RuleResult::Rejected)],
                    RuleResult::Accepted => vec![(pr.clone(), RuleResult::Accepted)],
                }
            }
            Rule::Move(res) => match res {
                RuleResult::Workflow(wf_name) => {
                    find_possible_ranges(pr.clone(), &wf_map[wf_name], wf_map)
                }
                RuleResult::Rejected => vec![(pr.clone(), RuleResult::Rejected)],
                RuleResult::Accepted => vec![(pr.clone(), RuleResult::Accepted)],
            },
        })
        .flatten()
        .collect_vec();
    res
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (input, (workflows, parts)) = parse_input(input).expect("Could not parse input");

    info!("{:?}", &workflows);
    info!("{:?}", parts);

    let workflow_map = workflows
        .into_iter()
        .map(|wf| (wf.name.clone(), wf))
        .collect::<HashMap<_, _>>();

    let initial_range = PartRange {
        x: 0..=4000,
        m: 0..=4000,
        a: 0..=4000,
        s: 0..=4000,
    };
    let all_ranges = find_possible_ranges(initial_range, &workflow_map["in"], &workflow_map)
        .into_iter()
        .filter(|(_, rr)| match rr {
            RuleResult::Accepted => true,
            _ => false,
        })
        .collect_vec();

    info!("{:?}", all_ranges);
    Ok("res".to_string())
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn test_process() -> miette::Result<()> {
        todo!("haven't built test yet");
        let input = "";
        assert_eq!("", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"px{a<2006:qkq,m>2090:A,rfg}
    pv{a>1716:R,A}
    lnx{m>1548:A,A}
    rfg{s<537:gd,x>2440:R,A}
    qs{s>3448:A,lnx}
    qkq{x<1416:A,crn}
    crn{x>2662:A,R}
    in{s<1351:px,qqz}
    qqz{s>2770:qs,m<1801:hdj,R}
    gd{a>3333:R,R}
    hdj{m>838:A,pv}
    
    {x=787,m=2655,a=1222,s=2876}
    {x=1679,m=44,a=2067,s=496}
    {x=2036,m=264,a=79,s=2244}
    {x=2461,m=1339,a=466,s=291}
    {x=2127,m=1623,a=2188,s=1013}"},
        "19114"
    )]
    #[case(indoc! {"px{a<2006:qkq,m>2090:A,rfg}
    pv{a>1716:R,A}
    lnx{m>1548:A,A}
    rfg{s<537:gd,x>2440:R,A}
    qs{s>3448:A,lnx}
    qkq{x<1416:A,crn}
    crn{x>2662:A,R}
    in{s<1351:px,qqz}
    qqz{s>2770:qs,m<1801:hdj,R}
    gd{a>3333:R,R}
    hdj{m>838:A,pv}
    
    {x=1679,m=44,a=2067,s=496}
    {x=2461,m=1339,a=466,s=291}"},
        "0"
    )]
    #[case(indoc! {"px{a<2006:qkq,m>2090:A,rfg}
    pv{a>1716:R,A}
    lnx{m>1548:A,A}
    rfg{s<537:gd,x>2440:R,A}
    qs{s>3448:A,lnx}
    qkq{x<1416:A,crn}
    crn{x>2662:A,R}
    in{s<1351:px,qqz}
    qqz{s>2770:qs,m<1801:hdj,R}
    gd{a>3333:R,R}
    hdj{m>838:A,pv}
    
    {x=2127,m=1623,a=2188,s=1013}"},
        "6951"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
