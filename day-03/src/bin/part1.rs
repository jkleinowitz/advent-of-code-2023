use ndarray::Array2;
use shared::{PartNumber, Point};

use std::fmt;

struct CurrentElement {
    pub chars: Vec<char>,
    pub pos: Point<i32>,
}

impl fmt::Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "('{}', {})", self.char, self.pos)
    }
}

#[derive(Debug)]
struct Symbol {
    char: char,
    pos: Point<i32>,
}

enum Elements {
    PartNumber,
    Symbol,
}

fn main() {
    let input = include_str!("./input1.txt");
    let result = part1(input);
    dbg!(result);
}

fn to_array(input: &str) -> Array2<char> {
    let chars: Vec<char> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .collect();
    let rows = input.lines().count();
    let cols = input.lines().next().unwrap().chars().count();
    Array2::from_shape_vec((rows, cols), chars).unwrap()
}

fn part1(input: &str) -> i32 {
    let arr = to_array(input);

    //let symbol_chars = ['*', '#', '$', '+'];

    let mut symbols: Vec<Symbol> = vec![];
    let mut part_numbers: Vec<PartNumber> = vec![];

    let mut row_index = -1;

    for row in arr.rows() {
        row_index += 1;
        let mut current_element: Option<CurrentElement> = None;
        for (x, c) in row.indexed_iter() {
            if current_element.is_some() && c.is_numeric() {
                let ele = current_element.as_mut().unwrap();
                ele.chars.push(c.clone());
            } else if current_element.is_some() && !c.is_numeric() {
                let ele = current_element.as_mut().unwrap();
                let number_str: String = ele.chars.iter().collect();
                let number = number_str.parse().unwrap();
                part_numbers.push(PartNumber {
                    pos: ele.pos,
                    number: number,
                });
                current_element = None;
            } else if current_element.is_none() && c.is_numeric() {
                current_element = Some(CurrentElement {
                    chars: vec![c.clone()],
                    pos: Point {
                        x: x as i32,
                        y: row_index,
                    },
                })
            }
            if !c.is_numeric() && *c != '.' {
                symbols.push(Symbol {
                    char: c.clone(),
                    pos: Point {
                        x: x as i32,
                        y: row_index,
                    },
                })
            }
        }
        if let Some(ele) = current_element.as_mut() {
            let number_str: String = ele.chars.iter().collect();
            let number = number_str.parse().unwrap();
            part_numbers.push(PartNumber {
                pos: ele.pos,
                number: number,
            });
        }
    }

    println!("Under the limit:");
    dbg!(&arr);
    println!("Symbols");
    //println!("{:#?}", symbols);
    println!("Part numbers");
    //println!("{:#?}", part_numbers);

    let parts_near_symbol: Vec<_> = part_numbers
        .iter()
        .filter(|part| {
            let pos = &part.pos;

            let lu = Point {
                x: pos.x - 1,
                y: pos.y - 1,
            };
            let rl = Point {
                x: pos.x + part.width() as i32,
                y: pos.y + 1,
            };
            symbols
                .iter()
                .map(|s| s.pos)
                .any(|p| p.x >= lu.x && p.x <= rl.x && p.y >= lu.y && p.y <= rl.y)
        })
        .collect();

    println!("Part numbers near symbol");
    //println!("{:#?}", parts_near_symbol);
    let sum_parts_near_symbol = parts_near_symbol.iter().map(|part| part.number).sum();
    sum_parts_near_symbol
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test1() {
        let input = r#"467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598.."#
            .trim();

        let result = part1(input);
        assert_eq!(result, 4361);
    }
}
