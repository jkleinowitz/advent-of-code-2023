use std::{
    collections::HashSet,
    fmt::{Display, Formatter},
};

use glam::IVec2;
use grid::Grid;
use itertools::Itertools;
use tracing::{debug, info};

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grid = MyMap(to_array(input));

    let mut beams = vec![Beam {
        path: vec![IVec2::new(-1, 0)],
        direction: BeamDirection::East,
        active: true,
    }];

    let mut split_origins: Vec<(IVec2, BeamDirection)> = Vec::new();

    while beams.iter().any(|b| b.active) {
        //info!("Beams: {:?}", beams);
        let mut new_beams = vec![];
        for beam in beams.iter_mut().filter(|b| b.active) {
            let pos = *beam.path.last().expect("Path should not be empty");
            let next_pos = match beam.direction {
                BeamDirection::North => pos + IVec2::new(0, -1),
                BeamDirection::South => pos + IVec2::new(0, 1),
                BeamDirection::East => pos + IVec2::new(1, 0),
                BeamDirection::West => pos + IVec2::new(-1, 0),
            };

            let next_elem = grid.0.get(next_pos.y as usize, next_pos.x as usize);
            //info!("({},{}): {:?}", next_pos.x, next_pos.y, next_elem);
            match next_elem {
                Some(Element::Empty) => {
                    beam.path.push(next_pos);
                }
                Some(Element::Mirror(mirror)) => {
                    beam.path.push(next_pos);
                    let new_direction = match mirror {
                        MirrorOrientation::NorthEastToSouthWest => match beam.direction {
                            BeamDirection::North => BeamDirection::East,
                            BeamDirection::South => BeamDirection::West,
                            BeamDirection::East => BeamDirection::North,
                            BeamDirection::West => BeamDirection::South,
                        },
                        MirrorOrientation::NorthWestToSouthEast => match beam.direction {
                            BeamDirection::North => BeamDirection::West,
                            BeamDirection::South => BeamDirection::East,
                            BeamDirection::East => BeamDirection::South,
                            BeamDirection::West => BeamDirection::North,
                        },
                        _ => todo!("other mirrors"),
                    };
                    beam.direction = new_direction;
                }
                Some(Element::Splitter(splitter)) => {
                    let new_directons = match splitter {
                        MirrorOrientation::Horizontal => match beam.direction {
                            BeamDirection::North | BeamDirection::South => {
                                vec![BeamDirection::East, BeamDirection::West]
                            }
                            _ => {
                                vec![]
                            }
                        },
                        MirrorOrientation::Vertical => match beam.direction {
                            BeamDirection::North | BeamDirection::South => {
                                vec![]
                            }
                            BeamDirection::East | BeamDirection::West => {
                                vec![BeamDirection::North, BeamDirection::South]
                            }
                        },
                        _ => todo!(),
                    };
                    beam.path.push(next_pos);
                    if !new_directons.is_empty() {
                        beam.active = false;
                        for new_direction in new_directons {
                            let origin = (next_pos, new_direction);
                            if split_origins.contains(&origin) {
                                break;
                            } else {
                                new_beams.push(Beam {
                                    //pos: next_pos,
                                    path: beam.path.clone(),
                                    direction: new_direction,
                                    active: true,
                                });
                                split_origins.push((next_pos, new_direction))
                            }
                        }
                    }
                }
                None => {
                    debug!("Beam stopped at {:?}", next_pos);
                    //info!("Path: {:?}", beam.path);
                    beam.active = false;
                }
            }
        }
        beams.append(&mut new_beams);
    }
    info!("Grid: \n{}", grid);
    //info!("Beams: \n{:?}", beams);

    beams.iter().enumerate().for_each(|(i, b)| {
        info!("Path: {i}");
        //grid.print_path(b.path.as_slice());
    });

    let path: Vec<IVec2> = beams
        .iter()
        .flat_map(|b| b.path.iter().map(|&p| p.clone()))
        .sorted_by_key(|a| (a.x, a.y))
        .dedup()
        .collect();
    info!("Combined path:");
    grid.print_path(path.as_slice());
    Ok((path.len() -1).to_string())
}

#[derive(Debug, Clone)]
struct Beam {
    //pos: IVec2,
    path: Vec<IVec2>,
    direction: BeamDirection,
    active: bool,
}

struct MyMap(Grid<Element>);

impl Display for MyMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in self.0.iter_rows() {
            for elem in row {
                write!(f, "{}", elem)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl MyMap {
    fn print_path(&self, path: &[IVec2]) {
        let mut grod_with_indices: String = String::new();
        let mut grid_with_path = String::new();
        for (y, row) in self.0.iter_rows().enumerate() {
            for (x, elem) in row.enumerate() {
                grod_with_indices.push_str(format!("({x}, {y})").as_str());
                if *elem == Element::Empty && path.contains(&&IVec2::new(x as i32, y as i32)) {
                    grid_with_path.push_str("#");
                } else {
                    grid_with_path.push_str(format!("{}", elem).as_str());
                }
            }
            grid_with_path.push_str("\n");
            grod_with_indices.push_str("\n");
        }
        println!("Path: \n{}", grid_with_path);
        //println!("\n{}", grod_with_indices);
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum MirrorOrientation {
    Horizontal,
    Vertical,
    NorthWestToSouthEast,
    NorthEastToSouthWest,
}
#[derive(Debug, Clone, Copy, PartialEq)]
enum BeamDirection {
    North,
    South,
    East,
    West,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Element {
    Empty,
    Mirror(MirrorOrientation),
    Splitter(MirrorOrientation),
}

impl TryFrom<char> for Element {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '\\' => Ok(Element::Mirror(MirrorOrientation::NorthWestToSouthEast)),
            '/' => Ok(Element::Mirror(MirrorOrientation::NorthEastToSouthWest)),
            '-' => Ok(Element::Splitter(MirrorOrientation::Horizontal)),
            '|' => Ok(Element::Splitter(MirrorOrientation::Vertical)),
            '.' => Ok(Element::Empty),
            _ => Err(format!("Could not parse char: {}", value)),
        }
    }
}
impl Display for Element {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Element::Empty => write!(f, "."),
            Element::Mirror(MirrorOrientation::NorthWestToSouthEast) => write!(f, "\\"),
            Element::Mirror(MirrorOrientation::NorthEastToSouthWest) => write!(f, "/"),
            Element::Splitter(MirrorOrientation::Horizontal) => write!(f, "-"),
            Element::Splitter(MirrorOrientation::Vertical) => write!(f, "|"),
            _ => write!(f, "?"),
        }
    }
}

fn to_array(input: &str) -> Grid<Element> {
    let chars: Vec<_> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .flat_map(Element::try_from)
        .collect();
    let cols = input.lines().next().unwrap().chars().count();

    debug!("Chars: {:?}", chars);

    Grid::from_vec(chars, cols)
}
#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn test_process() -> miette::Result<()> {
        let input: &str = indoc! {
        r#"\|...\....
        .|........
        ..........
        .-.../....
        .\.-./...."#};
        assert_eq!("", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {
        r#".|...\....
        |.-.\.....
        .....|-...
        ........|.
        ..........
        .........\
        ..../.\\..
        .-.-/..|..
        .|....-|.\
        ..//.|...."#},
        "46"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
