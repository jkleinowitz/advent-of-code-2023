use std::{cmp, collections::HashSet, str::FromStr};

use nom::{
    bytes::complete::tag,
    character::complete::{digit1, multispace1},
    combinator::map_res,
    multi::separated_list0,
    sequence::{pair, preceded, tuple},
    IResult,
};

use crate::custom_error::AocError;

#[tracing::instrument]
pub fn process(input: &str) -> miette::Result<String, AocError> {

    let line_count = input.lines().count();

    let cards: Vec<Card> = input
        .lines()
        .map(|l| l.trim())
        .flat_map(card_parser)
        .map(|(_, c)| c)
        .collect();

    dbg!(&cards);

    assert_eq!(line_count, cards.len());

    let sum: u32 = cards
        .iter()
        .map(|c| {
            let matches = c
                .my_numbers
                .intersection(&c.winning_numbers)
                .collect::<Vec<&u32>>()
                ;
            dbg!(&matches);
            let matches = matches.len();
            if matches > 0 {
                let base: u32 = 2;
                base.pow(matches as u32 - 1)
            } else {
                0
            }
        })
        .inspect(|v| {
            dbg!(v);
        })
        .sum();

    Ok(sum.to_string().to_string())
}

#[derive(Debug)]
struct Card {
    id: u32,
    winning_numbers: HashSet<u32>,
    my_numbers: HashSet<u32>,
}

fn card_parser(input: &str) -> IResult<&str, Card> {
    let (input, (id, winning_numbers, my_numbers)) = tuple((
        preceded(
            pair(tag("Card"), multispace1),
             map_res(digit1, FromStr::from_str)),
        preceded(
            pair(tag(":"), multispace1),
            separated_list0(multispace1, map_res(digit1, FromStr::from_str)),
        ),
        preceded(
            pair(tag(" |"), multispace1),
            separated_list0(multispace1, map_res(digit1, FromStr::from_str)),
        ),
    ))(input)?;

    let winning_numbers: HashSet<u32> = winning_numbers.into_iter().collect();
    let my_numbers: HashSet<u32> = my_numbers.into_iter().collect();

    Ok((
        input,
        Card {
            id,
            winning_numbers,
            my_numbers,
        },
    ))
}

#[cfg(test)]
mod tests {
    use super::*;

    use rstest::rstest;

    #[test]
    fn test_process() -> miette::Result<()> {
        let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
        Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
        Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
        Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
        Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
        Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
            .trim();
        assert_eq!("13", process(input)?);
        Ok(())
    }

    #[rstest]
    #[case("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53", 8)]
    #[case("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19", 2)]
    #[case("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1", 2)]
    #[case("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83", 1)]
    #[case("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36", 0)]
    #[case("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11", 0)]
    #[case("Card 174:  4 29 72 26 46 89 61 15 16 11 |  4 68 89 24 58 94 46 26 55 15 67 61 72 98 75 84 29 11 47  2 54 13  9 28 16", 512)]
    fn line_test(
        #[case] line: &str,
        #[case] expected: u32,
    ) {
        let res = process(line).expect("asdf");
        assert_eq!(expected, res.parse().unwrap())
    }
}
