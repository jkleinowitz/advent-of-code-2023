use std::collections::HashMap;

use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{alphanumeric1, newline},
    combinator::map,
    multi::separated_list1,
    sequence::{preceded, terminated, tuple},
    IResult,
};
use tracing::info;

use crate::custom_error::AocError;

#[derive(Debug)]
pub enum Direction {
    L,
    R,
}

impl From<char> for Direction {
    fn from(c: char) -> Self {
        match c {
            'L' => Direction::L,
            'R' => Direction::R,
            _ => panic!("Unknown direction"),
        }
    }
}

fn parse_directions(input: &str) -> Vec<Direction> {
    input.chars().map(Direction::from).collect_vec()
}

fn parser(input: &str) -> IResult<&str, &str> {
    alphanumeric1(input)
}

fn parse_first_line(input: &str) -> IResult<&str, Vec<Direction>> {
    terminated(map(parser, parse_directions), newline)(input)
}

fn parse_node(input: &str) -> IResult<&str, (&str, &str, &str)> {
    //AAA = (BBB, CCC)
    let (input, (node, _, left, _, right, _)) =
        tuple((parser, tag(" = ("), parser, tag(", "), parser, tag(")")))(input)?;

    Ok((input, (node, left, right)))
}

#[derive(Debug)]
struct IterState<'a> {
    i: i32,
    current: &'a str,
}

struct DynamicZip<I>
where
    I: Iterator,
{
    iterators: Vec<I>,
}

impl<I, T> Iterator for DynamicZip<I>
where
    I: Iterator<Item = T>,
{
    type Item = Vec<T>;
    fn next(&mut self) -> Option<Self::Item> {
        let output: Option<Vec<T>> = self.iterators.iter_mut().map(|iter| iter.next()).collect();
        output
    }
}

#[tracing::instrument(skip(input))]
pub fn process_brute_force(input: &str) -> miette::Result<String, AocError> {
    let (input, directions) = parse_first_line(input).expect("Could not get directions");

    let (_, nodes) = preceded(newline, separated_list1(newline, parse_node))(input)
        .expect("Could not parse nodes");

    let map = nodes
        .iter()
        .map(|chunk| (chunk.0, (chunk.1, chunk.2)))
        .collect::<HashMap<_, _>>();

    let keys_with_ending_with_a: Vec<_> = map.keys().filter(|&k| k.ends_with("A")).collect();

    info!("keys_with_ending_with_a: {:?}", keys_with_ending_with_a);

    let iterators: Vec<_> = keys_with_ending_with_a
        .iter()
        .map(|&k| {
            //let mut visited_nodes = vec![*k];
            let initial_value = IterState { i: 1, current: k };
            directions
                .iter()
                .clone()
                .cycle()
                .scan(initial_value, |acc, dir| {
                    let next = map.get(acc.current).expect("No next node present");

                    let next = match dir {
                        Direction::L => next.0,
                        Direction::R => next.1,
                    };
                    acc.i = acc.i + 1;
                    acc.current = next;

                    let next = IterState {
                        i: acc.i,
                        current: next,
                    };

                    Some(next)
                })
        })
        //.take(4)
        .collect();

    let dz = DynamicZip {
        iterators: iterators,
    };

    let res = dz
        .enumerate()
        .inspect(|(i, s)| {
            if i % 1000000 == 0 {
                info!(
                    "{}: {:?}, end_with_z: {}",
                    i,
                    s,
                    s.iter().filter(|y| y.current.ends_with("Z")).count()
                )
            }
        })
        .find(|(i, s)| s.iter().all(|y| y.current.ends_with("Z")));

    let res = res.expect("Did not find a path");
    let steps = res.0 + 1;
    info!("{:?}", res.1);

    Ok(steps.to_string())
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (input, directions) = parse_first_line(input).expect("Could not get directions");

    let (_, nodes) = preceded(newline, separated_list1(newline, parse_node))(input)
        .expect("Could not parse nodes");

    let map = nodes
        .iter()
        .map(|chunk| (chunk.0, (chunk.1, chunk.2)))
        .collect::<HashMap<_, _>>();

    let keys_with_ending_with_a: Vec<_> = map.keys().filter(|&k| k.ends_with("A")).collect();

    info!("keys_with_ending_with_a: {:?}", keys_with_ending_with_a);

    let iterators: Vec<_> = keys_with_ending_with_a
        .iter()
        .map(|&k| {
            let mut visited_nodes = vec![*k];
            let mut current_node = *k;
            let initial_value = IterState { i: 1, current: k };
            directions
                .iter()
                .clone()
                .cycle()
                .enumerate()
                .find_map(|(i, direction)| {
                    let next = map.get(current_node).expect("No next node present");
                    let next_node = match direction {
                        Direction::L => next.0,
                        Direction::R => next.1,
                    };
                    if next_node.ends_with("Z") {
                        Some(i + 1)
                    } else {
                        visited_nodes.push(next_node);
                        current_node = next_node;
                        None
                    }
                })
                .expect("should find a cycle")
        })
        .collect();
    let min_cycle = lcm(&iterators);

    Ok(min_cycle.to_string())
}

fn lcm(nums: &[usize]) -> usize {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(&nums[1..]);
    a * b / gcd_of_two_numbers(a, b)
}

fn gcd_of_two_numbers(a: usize, b: usize) -> usize {
    if b == 0 {
        return a;
    }
    gcd_of_two_numbers(b, a % b)
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        let input = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";
        assert_eq!("6", process(input)?);
        Ok(())
    }
    /*#[test_log::test]
        fn test_process2() -> miette::Result<()> {
            let input = "LLR

    AAA = (BBB, BBB)
    BBB = (AAA, ZZZ)
    ZZZ = (ZZZ, ZZZ)";
            assert_eq!("6", process(input)?);
            Ok(())
        }*/

    #[test_log::test(rstest)]
    //#[rstest]
    #[case("AAA = (BBB, BBB)",  ("AAA", "BBB", "BBB"))]
    #[case("CRF = (LVG, FNX)",  ("CRF", "LVG", "FNX"))]
    fn test_compare_line(#[case] input: &str, #[case] expected: (&str, &str, &str)) {
        let expected = Ok(("", expected));
        assert_eq!(expected, parse_node(input))
    }
}
