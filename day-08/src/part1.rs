use std::collections::HashMap;

use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, multispace0, newline},
    combinator::{map, not},
    multi::{many1, separated_list1},
    sequence::{preceded, terminated, tuple},
    IResult,
};
use tracing::{debug, info};

use crate::custom_error::AocError;

#[derive(Debug)]
pub enum Direction {
    L,
    R,
}

impl From<char> for Direction {
    fn from(c: char) -> Self {
        match c {
            'L' => Direction::L,
            'R' => Direction::R,
            _ => panic!("Unknown direction"),
        }
    }
}

fn parse_directions(input: &str) -> Vec<Direction> {
    input.chars().map(Direction::from).collect_vec()
}

fn parser(input: &str) -> IResult<&str, &str> {
    alpha1(input)
}

fn parse_first_line(input: &str) -> IResult<&str, Vec<Direction>> {
    terminated(map(parser, parse_directions), newline)(input)
}

fn parse_node(input: &str) -> IResult<&str, (&str, &str, &str)> {
    //AAA = (BBB, CCC)
    let (input, (node, _, left, _, right, _)) =
        tuple((alpha1, tag(" = ("), parser, tag(", "), parser, tag(")")))(input)?;

    Ok((input, (node, left, right)))
}

#[derive(Debug)]
struct ItState {
    i: usize,
    current: String,
    end: String,
}

#[derive(Debug)]
struct IterState<'a> {
    i: i32,
    current: &'a str,
    end: &'a str,
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (input, directions) = parse_first_line(input).expect("Could not get directions");

    let (_, nodes) = preceded(newline, separated_list1(newline, parse_node))(input)
        .expect("Could not parse nodes");

    let map = nodes
        .iter()
        .map(|chunk| (chunk.0, (chunk.1, chunk.2)))
        .collect::<HashMap<_, _>>();

    let start = "AAA";
    let end = "ZZZ";

    let initial_value = IterState {
        i: 1,
        current: start,
        end: end,
    };

    info!("{:?}", directions);
    info!("{:#?}", initial_value);

    let path = directions
        .iter()
        .clone()
        .cycle()
        .enumerate()
        .scan(initial_value, |acc, x| {
            let next = map.get(acc.current).expect("No next node present");

            let next = match x.1 {
                Direction::L => next.0,
                Direction::R => next.1,
            };
            acc.i = acc.i + 1;
            acc.current = next;

            let next = IterState {
                i: acc.i,
                current: next,
                end: acc.end,
            };
            if acc.i % 100000 == 0 {
                info!("{:?}", acc);
            }

            Some(next)
        })
        .take_while(|x| x.current != x.end)
        //.take(10)
        .last();
    info!("{:?}", path);

    let steps = path.expect("No elements").i;

    Ok(steps.to_string())
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        let input = "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";
        assert_eq!("2", process(input)?);
        Ok(())
    }
    #[test_log::test]
    fn test_process2() -> miette::Result<()> {
        let input = "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";
        assert_eq!("6", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case("AAA = (BBB, BBB)",  ("AAA", "BBB", "BBB"))]
    #[case("CRF = (LVG, FNX)",  ("CRF", "LVG", "FNX"))]
    fn test_compare_line(#[case] input: &str, #[case] expected: (&str, &str, &str)) {
        let expected = Ok(("", expected));
        assert_eq!(expected, parse_node(input))
    }

    #[test]
    fn test_compare() {
        let mut parser = not(parser);

        assert_eq!(parser(" = ("), Ok((" = (", ())));

        //let parser = tuple((alpha1, not(parser)));

        //assert_eq!(parser("asdf = ("), Ok((" = (", ())));
    }
}
