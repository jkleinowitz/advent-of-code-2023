use std::{collections::HashMap, process::Output};

use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, digit1, multispace0},
    combinator::map_res,
    multi::{separated_list0, separated_list1},
    sequence::{preceded, separated_pair},
    IResult,
};

#[derive(Debug)]
struct Game<'a> {
    id: u32,
    rounds: Vec<Round<'a>>,
}

#[derive(Debug)]
struct Round<'a> {
    colors: HashMap<&'a str, u32>,
}

fn game_parser(input: &str) -> IResult<&str, Game> {
    let (input, _) = tag("Game ")(input)?;
    let (input, id) = map_res(digit1, |digit_str: &str| digit_str.parse::<u32>())(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, rounds) = separated_list1(tag("; "), round_parser)(input)?;
    println!("{}", input);
    Ok((input, Game { id, rounds }))
}

fn round_parser(input: &str) -> IResult<&str, Round> {
    let (input, colors) = separated_list1(tag(", "), color_parser)(input)?;
    let map: HashMap<&str, u32> = colors.into_iter().map(|(a, b)| (b, a)).collect();
    Ok((input, Round { colors: map }))
}

fn color_parser(input: &str) -> IResult<&str, (u32, &str)> {
    separated_pair(map_res(digit1, str::parse), multispace0, alpha1)(input)
}

fn main() {
    let input = include_str!("./input1.txt");
    let result = part1(input);
    dbg!(result);
}


fn find_max_colors_each_round<'a>(game: Game<'a>) -> HashMap<&str, u32> {
    let all_colors: Vec<HashMap<&'a str, u32>>= game.rounds.iter().map(|round| round.colors.clone()).collect();

    let max_blue = all_colors.iter().map(|m| m.get("blue")).flatten().max().unwrap_or(&0);
    let max_red = all_colors.iter().map(|m| m.get("red")).flatten().max().unwrap_or(&0);
    let max_green = all_colors.iter().map(|m| m.get("green")).flatten().max().unwrap_or(&0);
    let mut min_colors: HashMap<&str, u32>  = HashMap::new();
    min_colors.insert("blue", *max_blue);
    min_colors.insert("red", *max_red);
    min_colors.insert("green", *max_green);

    min_colors
    
}

fn part1(input: &str) -> i32 {
    let r: Vec<_> = input
        .lines()
        .map(|line| {
            let result = game_parser(line).expect("Could not parse game");
            result.1
        })
        .map(|game| find_max_colors_each_round(game))
        .collect();

    println!("Values:");
    dbg!(&r);

    r.iter().map(|colors| {
        colors.get("blue").unwrap_or(&1) *
        colors.get("red").unwrap_or(&1) *
        colors.get("green").unwrap_or(&1)
    }).sum::<u32>() as i32
}


fn is_over_limit(game: &Game<'_>) -> bool {
    game.rounds.iter().all(|round| {
        let colors: &HashMap<&str, u32> = &round.colors;
        colors.get("blue").map_or(true, |v| v <= &14)
            && colors.get("red").map_or(true, |v| v <= &12)
            && colors.get("green").map_or(true, |v| v <= &13)
    })
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test1() {
        let result = part1(
            "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
        );
        assert_eq!(result, 2286);
    }

    #[test]
    fn test_game() {
        let res = game_parser("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        dbg!(res);
    }

    // function to test the round parser
    #[test]
    fn test_round() {
        let res = round_parser("3 blue, 4 red");
        dbg!(res);
    }

    #[test]
    fn test_color() {
        let res = color_parser("3 blue");
        dbg!(res);
    }
}
