use std::{iter::zip, str::FromStr};

use nom::{
    bytes::complete::tag,
    character::complete::{digit1, multispace1, newline},
    combinator::map_res,
    multi::many1,
    sequence::{preceded, separated_pair},
    IResult,
};
use tracing::info;

use crate::custom_error::AocError;

#[tracing::instrument]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    info!("Parsing");

    let (_, races) = parse_input(input).expect("Could not parse input");

    let res: usize = races
        .iter()
        .map(|r| {
            (0..=r.duration)
                .map(|speed| {
                    let remaining_time = r.duration - speed;
                    remaining_time * speed
                })
                .filter(|d| d > &r.record_distance)
                .count()
        })
        .product();
    Ok(res.to_string())
}

#[derive(Debug, PartialEq)]
struct Race {
    duration: i64,
    record_distance: i64,
}

fn parse_line(input: &str) -> IResult<&str, Vec<&str>> {
    many1(preceded(multispace1, digit1))(input)
}

fn parse_input(input: &str) -> IResult<&str, Vec<Race>> {
    let (input, (times, distances)) = separated_pair(
        preceded(tag("Time:"), parse_line),
        newline,
        preceded(tag("Distance:"), parse_line),
    )(input)?;

    let the_time = times.join("").parse().expect("Should be a number");
    let the_distance = distances.join("").parse().expect("Should be a number");
    let races = vec![Race {
        duration: the_time,
        record_distance: the_distance
    }];

    Ok((input, races))
}

#[cfg(test)]
mod tests {
    use tracing::{debug, warn};

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        println!("Testing");

        warn!("hi");

        let input = "Time:      7  15   30
Distance:  9  40  200";
        assert_eq!("71503", process(input)?);
        Ok(())
    }

    #[test_log::test]
    fn test_parse_race() {
        let input = "Time:      7  15   30
Distance:  9  40  200";

        let expected = Ok((
            "",
            vec![
                Race {
                    duration: 71530,
                    record_distance: 940200,
                },
            ],
        ));
        assert_eq!(expected, parse_input(input));
    }
}
