use std::{iter::zip, str::FromStr};

use nom::{
    bytes::complete::tag,
    character::complete::{digit1, multispace1, newline},
    combinator::map_res,
    multi::many1,
    sequence::{preceded, separated_pair},
    IResult,
};
use tracing::info;

use crate::custom_error::AocError;

#[tracing::instrument]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    info!("Parsing");

    let (_, races) = parse_input(input).expect("Could not parse input");

    let res: usize = races
        .iter()
        .map(|r| {
            (0..=r.duration)
                .map(|speed| {
                    let remaining_time = r.duration - speed;
                    remaining_time * speed
                })
                .filter(|d| d > &r.record_distance)
                .count()
        })
        .product();
    Ok(res.to_string())
}

#[derive(Debug, PartialEq)]
struct Race {
    duration: i32,
    record_distance: i32,
}

fn parse_line(input: &str) -> IResult<&str, Vec<i32>> {
    many1(preceded(multispace1, map_res(digit1, FromStr::from_str)))(input)
}

fn parse_input(input: &str) -> IResult<&str, Vec<Race>> {
    let (input, (times, distances)) = separated_pair(
        preceded(tag("Time:"), parse_line),
        newline,
        preceded(tag("Distance:"), parse_line),
    )(input)?;
    let races = zip(times, distances)
        .map(|(t, d)| Race {
            duration: t,
            record_distance: d,
        })
        .collect();

    Ok((input, races))
}

#[cfg(test)]
mod tests {
    use tracing::{debug, warn};

    use super::*;

    #[test_log::test]
    fn test_process() -> miette::Result<()> {
        println!("Testing");

        warn!("hi");

        let input = "Time:      7  15   30
Distance:  9  40  200";
        assert_eq!("288", process(input)?);
        Ok(())
    }

    #[test_log::test]
    fn test_parse_race() {
        let input = "Time:      7  15   30
Distance:  9  40  200";

        let expected = Ok((
            "",
            vec![
                Race {
                    duration: 7,
                    record_distance: 9,
                },
                Race {
                    duration: 15,
                    record_distance: 40,
                },
                Race {
                    duration: 30,
                    record_distance: 200,
                },
            ],
        ));
        assert_eq!(expected, parse_input(input));
    }
}
