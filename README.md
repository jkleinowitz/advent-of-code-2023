# Advent of Code 2023

Hi, this is my code for the 2023 edition of [Advent of Code](https://adventofcode.com/) and marks my fourth year of participating. This year, I've picked [Rust](https://www.rust-lang.org/) again because I really like the languge and become more fluent at it. 

## Things I learned this year

I learned about ...

* writing parser-combinators with [nom](https://docs.rs/nom/latest/nom/)
* how to setup logging and tracing with this [tracing crate](https://docs.rs/tracing/latest/tracing/) 
* how to generate beautiful [flamegraphs](https://github.com/flamegraph-rs/flamegraph).
* and much more

## Repo Structure

The structure of this repo is copied from the awesome [Chris Biscardi](https://github.com/ChristopherBiscardi/advent-of-code/tree/main/2023/rust) whose videos are a great source of knowledge.