use crate::custom_error::AocError;
use derivative::Derivative;
use glam::i32::IVec2;
use grid::Grid;
use pathfinding::prelude::dijkstra;
use tracing::{debug, info};

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grid = to_array(input);

    let goal: Pos = Pos {
        x: grid.cols() as isize - 1,
        y: grid.rows() as isize - 1,
        direction: IVec2::new(0, 0),
    };
    let start = Pos {
        x: 0,
        y: 0,
        direction: IVec2::new(4, 4),
    };

    let result: (Vec<Pos>, usize) = dijkstra(
        &start,
        |p| p.successors(&grid),
        |p| p.x == goal.x && p.y == goal.y && higher_than(&p.direction, 3),
    )
    .expect("there should be a path");

    let path: Vec<_> = result
        .0
        .iter()
        .map(|p| IVec2::new(p.x as i32, p.y as i32))
        .collect();

    result.0.iter().for_each(|p| info!("Pos: {:?}", p));
    info!("Path: {:?}", result.0);
    //info!("Visited: {:?}", already_visited);
    print_path(&grid, path.as_slice());
    Ok(result.1.to_string())
}

fn print_path(grid: &Grid<u32>, path: &[IVec2]) {
    let mut grod_with_indices: String = String::new();
    let mut grid_with_path = String::new();
    for (y, row) in grid.iter_rows().enumerate() {
        for (x, elem) in row.enumerate() {
            grod_with_indices.push_str(format!("({x}, {y})").as_str());
            if path.contains(&&IVec2::new(x as i32, y as i32)) {
                grid_with_path.push_str("#");
            } else {
                grid_with_path.push_str(format!("{}", elem).as_str());
            }
        }
        grid_with_path.push_str("\n");
        grod_with_indices.push_str("\n");
    }
    println!("Path: \n{}", grid_with_path);
    //println!("\n{}", grod_with_indices);
}

#[derive(Clone, Debug, Hash, Derivative)]
#[derivative(PartialEq)]
#[derivative(PartialOrd)]
#[derivative(Eq)]
#[derivative(Ord)]
struct Pos {
    x: isize,
    y: isize,
    #[derivative(PartialEq = "ignore")]
    #[derivative(PartialOrd = "ignore")]
    #[derivative(Ord = "ignore")]
    direction: IVec2,
}

fn is_direction_change(old: &IVec2, new: &IVec2) -> bool {
    old.x.abs() > 0 && new.x == 0
        || old.y.abs() > 0 && new.y == 0
        || old.x == 0 && new.x.abs() > 0
        || old.y == 0 && new.y.abs() > 0
}

fn higher_than(dir: &IVec2, min: i32) -> bool {
    dir.x.abs() > min || dir.y.abs() > min
}

fn lower_than(dir: &IVec2, max: i32)-> bool {
    dir.x < max || dir.y < max
}

// 1407 low
// 1418 nope
// 1428 high
// 1430 high
impl Pos {
    fn successors(&self, weights: &Grid<u32>) -> Vec<(Pos, usize)> {
        let cols = weights.cols() as isize;
        let rows = weights.rows() as isize;
        //dbg!(
        vec![self.left(), self.right(), self.up(), self.down()]
            .into_iter()
            .filter(|p| p.x >= 0 && p.x < cols && p.y >= 0 && p.y < rows)
            /* .inspect(|p| {
                debug!("old: {:?}, new: {:?}", self, p);
                debug!("\tis_direction_change: {}", is_direction_change(&self.direction, &p.direction));
                debug!("\thigher_than: {}", higher_than(&self.direction, 4));
                debug!(
                    "\tis_direction_change + higher_than: {}",
                    is_direction_change(&self.direction, &p.direction)
                        && higher_than(&self.direction, 3)
                );
            })*/
            .filter(|p| {
                if is_direction_change(&self.direction, &p.direction) {
                    higher_than(&self.direction, 3)
                } else {
                    true
                }
            })
            .filter(|p| p.direction.x.abs() <= 10 && p.direction.y.abs() <= 10)
            //.filter(|p| p.x != self.x || p.y != self.y)
            .map(|p| {
                let (x, y) = (p.x, p.y);
                (
                    p,
                    weights
                        .get(y, x)
                        .expect("Could not find position")
                        .clone()
                        .try_into()
                        .unwrap(),
                )
            })
            .collect()
        //)
    }
    fn left(&self) -> Pos {
        let v: i32 = -1;
        Pos {
            x: self.x + v as isize,
            y: self.y,
            direction: if self.direction.y != 0 {
                IVec2 { x: v, y: 0 }
            } else {
                IVec2 {
                    x: self.direction.x + v,
                    y: 0,
                }
            },
        }
    }
    fn right(&self) -> Pos {
        let v: i32 = 1;
        Pos {
            x: self.x + v as isize,
            y: self.y,
            direction: if self.direction.y != 0 {
                IVec2 { x: v, y: 0 }
            } else {
                IVec2 {
                    x: self.direction.x + v,
                    y: 0,
                }
            },
        }
    }
    fn up(&self) -> Pos {
        let v: i32 = -1;
        Pos {
            x: self.x,
            y: self.y + v as isize,
            direction: if self.direction.x != 0 {
                IVec2 { x: 0, y: v }
            } else {
                IVec2 {
                    x: 0,
                    y: self.direction.y + v,
                }
            },
        }
    }
    fn down(&self) -> Pos {
        let v: i32 = 1;
        Pos {
            x: self.x,
            y: self.y + v as isize,
            direction: if self.direction.x != 0 {
                IVec2 { x: 0, y: v }
            } else {
                IVec2 {
                    x: 0,
                    y: self.direction.y + v,
                }
            },
        }
    }
}

fn to_array(input: &str) -> Grid<u32> {
    let chars: Vec<_> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .flat_map(|c| c.to_digit(10))
        .collect();
    let cols = input.lines().next().unwrap().chars().count();
    Grid::from_vec(chars, cols)
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    //#[test]
    fn test_process() -> miette::Result<()> {
        todo!("haven't built test yet");
        let input = "";
        assert_eq!("", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"2413432311323
    3215453535623
    3255245654254
    3446585845452
    4546657867536
    1438598798454
    4457876987766
    3637877979653
    4654967986887
    4564679986453
    1224686865563
    2546548887735
    4322674655533"},
        "94"
    )]
    #[case(indoc! {"111111111111
    999999999991
    999999999991
    999999999991
    999999999991"},
        "71"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
