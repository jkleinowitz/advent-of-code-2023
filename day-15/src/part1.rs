use tracing::debug;

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let res: usize = input.split(",").map(|step| hash(step)).sum();

    Ok(res.to_string())
}

fn hash(input: &str) -> usize {
    debug!("Step: {}", input);
    input.chars().fold(0, |acc: usize, c| {
        let ascii_code = c as u32;
        ((acc + ascii_code as usize) * 17) % 256
    })
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test]
    fn test_process() -> miette::Result<()> {
        let input = "HASH";
        assert_eq!("52", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"},
        "1320"
    )]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        // 520606 too high
        assert_eq!(expected, process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"HASH"}, 52)]
    #[case(indoc! {"rn=1"}, 30)]
    #[case(indoc! {"cm-"}, 253)]
    #[case(indoc! {"qp=3"}, 97)]
    #[case(indoc! {"cm=2"}, 47)]
    #[case(indoc! {"qp-"}, 14)]
    #[case(indoc! {"pc=4"}, 180)]
    #[case(indoc! {"ot=9"}, 9)]
    #[case(indoc! {"ab=5"}, 197)]
    #[case(indoc! {"pc-"}, 48)]
    #[case(indoc! {"pc=6"}, 214)]
    #[case(indoc! {"ot=7"}, 231)]
    fn test_hash(#[case] input: &str, #[case] expected: usize) -> miette::Result<()> {
        assert_eq!(expected, hash(input));
        Ok(())
    }
}
