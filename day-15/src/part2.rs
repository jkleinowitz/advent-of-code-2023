use std::{collections::HashMap, fmt::Display};

use tracing::{debug, info};
use tracing_subscriber::field::debug;

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let commands: Vec<_> = input
        .split(",")
        .map(|step| {
            if let Some((label, focal_length)) = step.split_once("=") {
                let hash = hash(label);

                Operation::Insert(Insert {
                    hash,
                    lens: Lens {
                        label: label.to_string(),
                        focal_length: focal_length.parse().expect("Could not parse focal length"),
                    },
                })
            } else if let Some((label, _)) = step.split_once("-") {
                let hash = hash(label);

                Operation::Remove(Remove {
                    hash,
                    label: label.to_string(),
                })
            } else {
                panic!("Could not parse step")
            }
        })
        .collect();

    let mut boxes = Vec::new();
    (0..256).for_each(|_| boxes.push(Vec::new()));

    let init = LavaFacility {
        boxes,
        locations: HashMap::new(),
    };
    info!("Commands: {:?}", commands);

    let res = commands.iter().fold(init, |mut acc, cmd| {
        match cmd {
            Operation::Insert(insert_data) => {
                info!("Insert: hash: {}, {}", insert_data.hash, insert_data.lens);
                if let Some((box_idx, order)) = acc.locations.get(&insert_data.lens.label) {
                    let lens_box = acc.boxes.get_mut(*box_idx).expect("Box should exist");
                    lens_box[*order] = insert_data.lens.clone();
                    //acc.locations
                    //    .insert(insert_data.lens.label.clone(), (*box_idx, *order));
                } else {
                    let lens_box = acc
                        .boxes
                        .get_mut(insert_data.hash)
                        .expect("Box should exist");
                    let lens = &insert_data.lens;
                    lens_box.push(lens.clone());
                    acc.locations
                        .insert(lens.label.clone(), (insert_data.hash, (lens_box.len() - 1)));
                }
            }
            Operation::Remove(remove_data) => {
                info!(
                    "Remove: hash: {}, label: {}",
                    remove_data.hash, remove_data.label
                );
                if let Some((box_idx, order)) = acc.locations.get(&remove_data.label) {
                    let lens_box = acc.boxes.get_mut(*box_idx).expect("Box should exist");

                    lens_box.remove(*order);

                    lens_box[*order..lens_box.len()].iter().for_each(|lens| {
                        let idx = acc
                            .locations
                            .get_mut(lens.label.as_str())
                            .expect("Lens should exist");

                        debug!("adjusting lens: {}, idx: {:?}", lens.label, idx);
                        idx.1 -= 1;
                    });
                    acc.locations.remove(&remove_data.label);
                } else {
                    info!("label not found: {}", &remove_data.label)
                }
            }
        }
        let non_empty_boxes = acc
            .boxes
            .iter()
            .filter(|b| !b.is_empty())
            .collect::<Vec<_>>();
        info!("acc: \n{}locations: \n{:?}", acc, acc.locations);
        acc
    });

    info!("\n{}", res);

    let total: usize = res
        .boxes
        .iter()
        .enumerate()
        .filter(|(i, b)| !b.is_empty())
        .map(|(i, b)| {
            let box_sum: usize = b
                .iter()
                .enumerate()
                .map(|(li, l)| l.focal_length as usize * (li + 1))
                .sum();
            box_sum * (i + 1)
        })
        .sum();

    Ok(total.to_string())
}

#[derive(Debug)]
struct Insert {
    hash: usize,
    lens: Lens,
}

#[derive(Debug)]
struct Remove {
    hash: usize,
    label: String,
}

#[derive(Debug)]
enum Operation {
    Insert(Insert),
    Remove(Remove),
}

#[derive(Debug, Clone)]
struct Lens {
    label: String,
    focal_length: u8,
}

impl Display for Lens {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{} {}]", self.label, self.focal_length)
    }
}

#[derive(Debug)]
struct LavaFacility {
    boxes: Vec<Vec<Lens>>,
    locations: HashMap<String, (usize, usize)>,
}
impl Display for LavaFacility {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut res = String::new();
        for (idx, box_) in self.boxes.iter().enumerate().filter(|(_, b)| !b.is_empty()) {
            res.push_str(&format!("Box {}: ", idx));
            for lens in box_ {
                res.push_str(&format!("[{} {}] ", lens.label, lens.focal_length));
            }
            res.push_str("\n");
        }
        write!(f, "{}", res)
    }
}

fn hash(input: &str) -> usize {
    debug!("Step: {}", input);
    input.chars().fold(0, |acc: usize, c| {
        let ascii_code = c as u32;
        ((acc + ascii_code as usize) * 17) % 256
    })
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test_log::test(rstest)]
    #[case(indoc! {"rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"},
        "145"
    )]
    fn test_process(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        // 520606 too high
        assert_eq!(expected, process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    #[case(indoc! {"HASH"}, 52)]
    #[case(indoc! {"rn=1"}, 30)]
    #[case(indoc! {"cm-"}, 253)]
    #[case(indoc! {"qp=3"}, 97)]
    #[case(indoc! {"cm=2"}, 47)]
    #[case(indoc! {"qp-"}, 14)]
    #[case(indoc! {"pc=4"}, 180)]
    #[case(indoc! {"ot=9"}, 9)]
    #[case(indoc! {"ab=5"}, 197)]
    #[case(indoc! {"pc-"}, 48)]
    #[case(indoc! {"pc=6"}, 214)]
    #[case(indoc! {"ot=7"}, 231)]
    fn test_hash(#[case] input: &str, #[case] expected: usize) -> miette::Result<()> {
        assert_eq!(expected, hash(input));
        Ok(())
    }
}
