use difference::{Changeset, Difference};
use grid::Grid;
use itertools::Itertools;
use tracing::{debug, info};

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grids = parse(input);

    let res: usize = grids.iter().map(find_reflection).sum();

    Ok(res.to_string())
}

fn parse(input: &str) -> Vec<Grid<char>> {
    input
        .split("\n\n")
        .inspect(|l| info!("\n{}", l))
        .map(to_array)
        .collect_vec()
}

#[tracing::instrument]
fn compare_lane(lane_a: &[String], lane_b: &[String]) -> bool {
    let a = lane_a.iter().rev();
    let b = lane_b.iter();
    let sum: usize = a
        .zip(b)
        //.inspect(|(a, b)| debug!("{} == {}: {}", a, b, a == b))
        .map(|(s1, s2)| Changeset::new(s1, s2, ""))
        .inspect(|cs| info!("{}", cs))
        //.map(|cs| cs.distance as usize)
        .flat_map(|cs| {
            cs.diffs
                .iter()
                .tuple_windows()
                .filter_map(|(a, b)| match (a, b) {
                    (Difference::Add(texta), Difference::Rem(textb)) => {
                        Some(texta.len() + textb.len())
                    }
                    (Difference::Rem(texta), Difference::Add(textb)) => {
                        Some(texta.len() + textb.len())
                    }
                    (_, _) => None,
                })
                .collect_vec()
        })
        .sum();
    sum == 2
}
#[tracing::instrument]
fn compare_lane_full(lane_a: &[String], lane_b: &[String]) -> bool {
    let a = lane_a.iter().rev().join("\n");
    let b = lane_b.iter().join("\n");

    let cs = Changeset::new(&a, &b, "");
    info!("{}", &cs);

    let sum: usize = cs
        .diffs
        .iter()
        .filter_map(|diff| match diff {
            Difference::Same(_) => None,
            Difference::Add(text) => Some(text.len()),
            Difference::Rem(text) => Some(text.len()),
        })
        .sum();
    sum == 2
}

fn find_reflection(grid: &Grid<char>) -> usize {
    let cols: Vec<String> = grid.iter_cols().map(|it| it.collect::<String>()).collect();
    let col = (1..cols.len()).find(|i| {
        let (a, b) = cols.split_at(*i);
        compare_lane(a, b)
    });

    let rows: Vec<String> = grid.iter_rows().map(|it| it.collect::<String>()).collect();
    let row = (1..rows.len())
        .find(|i| {
            let (a, b) = rows.split_at(*i);
            compare_lane(a, b)
        })
        .map(|v| v * 100);

    info!("col: {:?}, row: {:?}", col, row);
    //let res = col.(row).expect("At least one");

    //25361
    //29961
    //29941

    //let res = col.unwrap_or(0) + row.unwrap_or(0);
    let res = row.or(col).expect("at least one");
    info!("res: {}", res);
    res
}

fn to_array(input: &str) -> Grid<char> {
    let chars: Vec<char> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .collect();
    let cols = input.lines().next().unwrap().chars().count();
    let grid = Grid::from_vec(chars, cols);
    grid
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use rstest::rstest;

    use super::*;

    #[test_log::test(test)]
    fn test_process() -> miette::Result<()> {
        let input = "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";
        assert_eq!("400", process(input)?);
        Ok(())
    }

    #[test_log::test(rstest)]
    //#[rstest]
    #[case(
        ".........####.##.
.#..#...#.##.###.
.#..#.#..##.#####
.#####.......##.#
#.##.###..##.####
.####....#..##.#.
...............#.
######.#..###..#.
.####..#.#......#
.####..#.#......#
######.#..###..#.",
        "3"
    )]
    #[case(indoc! {"#..#..##.#.#.##
.##..##......##
.#...#.########
#..#..##..#...#
#..#..##..#...#
.#...#.########
.##..##......##
#..#..##.#.#.##
..####.##.##..#
##.#.####...#..
##.#.####...#..
..####.##.##..#
...#..##.#.#.##"},
        "1000"
    )]
    #[case(
        indoc! {"###..##..##..##
#..#.#.##.#.#..
##..#######...#
##....####....#
##.#..####..#.#
######....#####
.##..........##
#.....#..#.....
####.##..##.###
####.##..##.###
#.....#..#.....
.##..........##
######....#####"},
        "8"
    )]
    #[case(
        indoc! {"###.#..####..#.##
..#####....#####.
...#....##....#..
.#..###....###..#
...#.#.####.#.#..
###.#..####..#.##
###.##.#..#.##.##"},
        "1"
    )]
    #[case(
        indoc! {"###....###.###...
        ..#.#..####.###.#
        ..#.#..####.###.#
        ###.#..###.###...
        #.##...#.####....
        #.#..#####....#.#
        ######..#.....#.#
        #.#..#..##.#.....
        #.#..#..##.#.....
        ######..#.....#.#
        #.#..#####....#.#
        #.##...#.####....
        ###.#..###.###...
        ..#.#..####.###.#
        ..#.#..####.###.#
        ###....###.###...
        ....#..##...##.##"},
        "200"
    )]
    #[case(indoc! {"#####.##.########
    .#..#...##......#
    ..#.###.#........
    ..#.###.#........
    .#.##...##......#
    #####.##.########
    .###.#.#...#..#..
    .####.#.#.#.##.#.
    .##.##.#.##....##"},
        "300"
    )]
    #[case(indoc! {".####..##....
    ......#...###
    ..##..#...###
    .......###...
    #....####.###
    .#####...#.##
    #....####....
    .........####
    .#..#...#.#..
    ..##..#...#..
    #....##..#.##
    ........#..##
    .#..#.##.....
    ......#.###..
    #....#..#.#..
    ##..##..#..##
    ######....###"},
        "3"
    )]
    #[case(indoc! {"....####.#####...
    ..##..###.##.##.#
    ..##..###.##.##.#
    ....####.#####...
    .....#.####.#.#.#
    ###.#####.####...
    .#...#..##.#..###
    ##.....#.#.###...
    ..##.##..#..###..
    ####.#.#...#.##..
    ##.###..#.#....##
    ...##...#..##..#.
    ..#.####.##......
    ##.######....####
    ....#.#..#####..."},
        "1"
    )]
    #[case(indoc! {"......#
    ###.#..
    ###.##.
    ###.##.
    ###.#..
    .....##
    ##..#..
    ##.#...
    .###.#.
    ##.....
    ..#...#
    #....##
    #....##
    ..#...#
    ##....."},
        "300"
    )]
    #[case(indoc! {"######...###.
    ##########.##
    ..##....#####
    ..##..###.#..
    ..##..#.#.#..
    ......#......
    .#..#..#.#..#
    ..##..#...##.
    #....#.#..##.
    ######.#####.
    .......##..#.
    .......##..#.
    ######.######"},
        "1100"
    )]
    #[case(indoc! {"####.#.#......#
    #.####.#..##.#.
    ###.###.###...#
    ####.#.##....##
    .###..#.##..###
    #....#....###.#
    #...###..#.....
    ##..###..#.....
    #....#....###.#
    .###..#.##..###
    ####.#.##....##
    ###.###.###...#
    #.####.#..##.#.
    ####.#.#......#
    #...#.#.#..#.#.
    #.##.##.#....##
    #.##.##.#....##"},
        "700"
    )]
    #[case(indoc! {"....##.....##.###
    ....##.....##.###
    .##.###.####....#
    ..#.....#.....#..
    #..###.#.....#..#
    #..##...#....##.#
    #..#.#...####.#.#
    #..##.#.#..#.##..
    .##.......#######
    .##.#...##.#..#..
    .##.#...#.####...
    ....#.#.#.#.#..#.
    .......#.#...####
    ....###..#.#...##
    ....#....#..#...#
    .##..#.##...#.##.
    ######.##.#..#..."},
        "2"
    )]
    #[case(indoc! {"####.########.#
    #####.#.##.#.##
    ##.#..........#
    #####..####..##
    ....#.##..##.#.
    ....##.####.##.
    #..##.#....#.##
    .##...#....#...
    #..#.#..##..#.#"},
        "2"
    )]
    #[case(indoc! {"....##.....##.###
    ....##.....##.###
    .##.###.####....#
    ..#.....#.....#..
    #..###.#.....#..#
    #..##...#....##.#
    #..#.#...####.#.#
    #..##.#.#..#.##..
    .##.......#######
    .##.#...##.#..#..
    .##.#...#.####...
    ....#.#.#.#.#..#.
    .......#.#...####
    ....###..#.#...##
    ....#....#..#...#
    .##..#.##...#.##.
    ######.##.#..#...
    
    ####.########.#
    #####.#.##.#.##
    ##.#..........#
    #####..####..##
    ....#.##..##.#.
    ....##.####.##.
    #..##.#....#.##
    .##...#....#...
    #..#.#..##..#.#"},
        "4"
    )]
    #[test_log::test(test)]
    fn test_process2(#[case] input: &str, #[case] expected: &str) -> miette::Result<()> {
        assert_eq!(expected, process(input)?);
        Ok(())
    }
}
