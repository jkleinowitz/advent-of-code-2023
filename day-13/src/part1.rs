use grid::Grid;
use itertools::Itertools;
use tracing::{debug, info};

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let grids = parse(input);

    let res: usize = grids.iter().map(find_reflection).sum();

    Ok(res.to_string())
}

fn parse(input: &str) -> Vec<Grid<char>> {
    input
        .split("\n\n")
        .inspect(|l| info!("{}", l))
        .map(to_array)
        .collect_vec()
}

#[tracing::instrument]
fn compare_lane(lane_a: &[String], lane_b: &[String]) -> bool {
    let a = lane_a.iter().rev();
    let b = lane_b.iter();
    let res = a
        .zip(b)
        .inspect(|(a, b)| debug!("{} == {}: {}", a, b, a == b))
        .all(|(s1, s2)| s1 == s2);
    //info!("Result: {}", res);
    res
}

fn find_reflection(grid: &Grid<char>) -> usize {
    let cols: Vec<String> = grid.iter_cols().map(|it| it.collect::<String>()).collect();
    let col = (1..cols.len()).find(|i| {
        let (a, b) = cols.split_at(*i);
        compare_lane(a, b)
    });

    let rows: Vec<String> = grid.iter_rows().map(|it| it.collect::<String>()).collect();
    let row = (1..rows.len()).find(|i| {
        let (a, b) = rows.split_at(*i);
        compare_lane(a, b)
    });

    let res = col.unwrap_or(0) + (row.unwrap_or(0) * 100);
    res
}

fn to_array(input: &str) -> Grid<char> {
    let chars: Vec<char> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .collect();
    let cols = input.lines().next().unwrap().chars().count();
    let grid = Grid::from_vec(chars, cols);
    grid
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test_log::test(test)]
    fn test_process() -> miette::Result<()> {
        let input = "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";
        assert_eq!("405", process(input)?);
        Ok(())
    }
}
