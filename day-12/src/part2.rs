use crate::custom_error::AocError;
use itertools::{repeat_n, Itertools};
use nom::{
    bytes::complete::{is_a, take_till, take_while},
    character::{
        complete::{char, digit1, multispace1, newline},
        is_alphabetic,
    },
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};
use tracing::info;

use std::str::FromStr;

fn input_parser(input: &str) -> IResult<&str, Vec<(&str, Vec<u32>)>> {
    separated_list1(newline, line_parser)(input)
}

fn line_parser(input: &str) -> IResult<&str, (&str, Vec<u32>)> {
    separated_pair(
        is_a("?.#"),
        multispace1,
        separated_list1(char(','), map_res(digit1, FromStr::from_str)),
    )(input)
}

#[tracing::instrument]
fn check_option(option: &str, line: &str, batches: &Vec<u32>) -> bool {
    let mut option_iter = option.chars();
    let filled_option = line
        .chars()
        .map(|c| match c {
            '?' => option_iter
                .next()
                .expect("should have a length similar to needed gaps"),
            value => value,
        })
        .collect::<String>();
    //info!(filled_option);
    let counts = filled_option
        .chars()
        .group_by(|c| c == &'#')
        .into_iter()
        .filter_map(|(is_hashes, group)| is_hashes.then_some(group.into_iter().count() as u32))
        .collect::<Vec<u32>>();
    //info!(?counts);
    *batches == counts
}

fn possible_solution_count(spaces_to_fill: usize, line: &str, batches: &Vec<u32>) -> usize {
    let options = generate_permutations(spaces_to_fill);
    let count = options
        .filter(|option| check_option(option, line, batches))
        .count();
    count
}

fn generate_permutations(spaces_to_fill: usize) -> impl Iterator<Item = String> {
    repeat_n([".", "#"].into_iter(), spaces_to_fill as usize)
        .multi_cartesian_product()
        .map(|v| v.join(""))
}

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (_input, data) = input_parser(input).expect("Could not parse input");
    let res: usize = data
        .iter()
        .map(|(line, buckets)| {
            info!("line: '{}', buckets: '{:?}'", line, buckets);
            let line = repeat_n(line, 5).join("?");
            let buckets = buckets.repeat(5);
            info!("line: '{}', buckets: '{:?}'", line, buckets);
            let spaces_to_fill = line.chars().filter(|c| *c == '?').count();

            possible_solution_count(spaces_to_fill, &line, &buckets)
        })
        .sum();
    Ok(res.to_string())
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::*;

    #[test]
    fn test_process() -> miette::Result<()> {
        let input = "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";
        assert_eq!("21", process(input)?);
        Ok(())
    }
}
