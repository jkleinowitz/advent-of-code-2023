use std::{ops::Range, str::FromStr};

use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, digit1, multispace0, multispace1, newline},
    combinator::map_res,
    multi::{many0, many1, many_m_n, separated_list0},
    sequence::{preceded, separated_pair, terminated},
    IResult,
};

use crate::custom_error::AocError;

#[derive(PartialEq, Debug)]
struct SourceDestinationMap {
    source: String,
    destination: String,
    values: Vec<MapEntry>,
}

#[derive(PartialEq, Debug)]
struct MapEntry {
    source_range_start: i64,
    destination_range_start: i64,
    range_length: i64,
}

impl MapEntry {
    fn source_range(&self) -> Range<i64> {
        self.source_range_start..(self.source_range_start + self.range_length)
    }
    fn destination_range(&self) -> Range<i64> {
        self.destination_range_start..(self.destination_range_start + self.range_length)
    }
}

#[derive(PartialEq, Debug)]
struct SeedData {
    seeds: Vec<Range<i64>>,
    mappings: Vec<SourceDestinationMap>,
}

fn parse_map_entry(input: &str) -> IResult<&str, MapEntry> {
    let (input, values) = many_m_n(
        3,
        3,
        preceded(multispace0, map_res(digit1, FromStr::from_str)),
    )(input)?;
    //let (input, values) = separated_list1(multispace1, map_res(digit1, FromStr::from_str))(input)?;

    let destination_range_start: i64 = *values.get(0).expect("msg");
    let source_range_start: i64 = *values.get(1).expect("msg");
    let range_length: i64 = *values.get(2).expect("msg");

    Ok((
        input,
        MapEntry {
            source_range_start,
            destination_range_start,
            range_length,
        },
    ))
}

fn parse_source_destination_map(input: &str) -> IResult<&str, SourceDestinationMap> {
    let (input, ((from, to), values)) = separated_pair(
        separated_pair(alpha1, tag("-to-"), alpha1),
        tag(" map:"),
        preceded(newline, separated_list0(newline, parse_map_entry)),
    )(input)?;

    Ok((
        input,
        SourceDestinationMap {
            source: from.into(),
            destination: to.into(),
            values,
        },
    ))
}

fn parse_seeds(input: &str) -> IResult<&str, Vec<Range<i64>>> {
    let (input, seeds) = preceded(
        terminated(tag("seeds:"), multispace0),
        many1(
            preceded(multispace0,
            separated_pair(
            map_res(digit1, FromStr::from_str),
            multispace1,
            map_res(digit1, FromStr::from_str),
        ))),
    )(input)?;
    //let seed_list: Vec<(i64, i64)> = seeds;
    let seeds = seeds.iter().map(|(i, j)| {
        let start: &i64 = i;
        let fin: &i64 = j;
        *start..(start + fin)
    }).collect();

    Ok((input, seeds))
}

fn parse_seed_data(input: &str) -> IResult<&str, SeedData> {
    let (input, seeds) = terminated(parse_seeds, many0(newline))(input)?;
    let (input, mappings) = separated_list0(many1(newline), parse_source_destination_map)(input)?;

    Ok((input, SeedData { seeds, mappings }))
}

#[tracing::instrument]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let (_, seed_data) = parse_seed_data(input).unwrap();

    let seed_ranges = seed_data.seeds;
    let mappings = seed_data.mappings;

    let mut min_value = i64::MAX;


    println!("Seeds: {:?}", seed_ranges);

    for seed_range in seed_ranges {

        let no_elements = seed_range.end - seed_range.start;
        println!("Checking seed range: {:?}: {} elements", seed_range, no_elements);
        for (index, seed) in seed_range.enumerate() {
            if index % 500000 == 0 {
                let progress = (index as f64 / no_elements as f64) * 100.0;
                println!("Checking seed: {}, progress: {:.2}%", seed, progress);
            }
            let mut current_val = seed;
            for ele in mappings.iter() {
                //println!("Checking from {} to {}", &ele.source, &ele.destination);
                for mapping in ele.values.iter() {
                    let rng: Range<i64> = mapping.source_range();
                    if rng.contains(&current_val) {
                        let i = current_val - rng.start;
                        let new_val = mapping.destination_range().start + i;
                        //println!("Seed {:?}: Moving from {} to {}", seed, current_val, new_val);
                        current_val = new_val;
                        break;
                    }
                }
            }
            //println!("Final value for {:?}: {}", seed, current_val);
            min_value = min_value.min(current_val);
        }
    }

    println!("Minium value: {}", min_value);

    Ok(min_value.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    use rstest::rstest;

    #[test]
    fn test_process() -> miette::Result<()> {
        let input = r#"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"#;
        assert_eq!("46", process(input)?);
        Ok(())
    }

    #[test]
    fn test_parse_map() {
        let input = "seed-to-soil map:
50 98 2
52 50 48";
        let expected = Ok((
            "",
            SourceDestinationMap {
                source: "seed".to_string(),
                destination: "soil".to_string(),
                values: vec![
                    MapEntry {
                        source_range_start: 50,
                        destination_range_start: 98,
                        range_length: 2,
                    },
                    MapEntry {
                        source_range_start: 52,
                        destination_range_start: 50,
                        range_length: 48,
                    },
                ],
            },
        ));
        assert_eq!(parse_source_destination_map(input), expected);
    }

    #[test]
    fn test_parse_map_entry() {
        let input = "50 98 2";
        let expected = Ok((
            "",
            MapEntry {
                source_range_start: 50,
                destination_range_start: 98,
                range_length: 2,
            },
        ));
        assert_eq!(parse_map_entry(input), expected);
    }

    #[rstest]
    #[case("seeds: 79 14 55 13", vec![79..93, 55..68])]
    #[case("seeds:  3640772818 104094365", vec![3640772818..3744867183])]
    #[case("seeds: 3640772818 104094365 1236480411 161072229 376099792 370219099 1590268366 273715765 3224333694 68979978 2070154278 189826014 3855332650 230434913 3033760782 82305885 837883389 177854788 2442602612 571881366", 
    vec![3640772818..3744867183, 1236480411..1397552640, 376099792..746318891, 1590268366..1863984131, 3224333694..3293313672, 2070154278..2259980292, 3855332650..4085767563, 3033760782..3116066667, 837883389..1015738177, 2442602612..3014483978])]
    fn test_parse_seedsy(#[case] input: &str, #[case] expected: Vec<Range<i64>>) {
        let expected = Ok(("", expected));
        assert_eq!(parse_seeds(input), expected);
    }

    #[test]
    fn test_parse_mappings() {
        let input = r#"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48"#;

        let expected_mapping = SeedData {
            seeds: vec![79..93, 55..68],
            mappings: vec![SourceDestinationMap {
                source: "seed".to_string(),
                destination: "soil".to_string(),
                values: vec![
                    MapEntry {
                        source_range_start: 50,
                        destination_range_start: 98,
                        range_length: 2,
                    },
                    MapEntry {
                        source_range_start: 52,
                        destination_range_start: 50,
                        range_length: 48,
                    },
                ],
            }],
        };
        let expected = Ok(("", expected_mapping));
        assert_eq!(parse_seed_data(input), expected);
    }
}
