use std::{
    fmt::{self},
    iter::Map,
    usize,
};

use ndarray::{iter::IndexedIter, Array2, IndexLonger};

use tracing::{debug, info};

use crate::custom_error::AocError;

#[tracing::instrument(skip(input))]
pub fn process(input: &str) -> miette::Result<String, AocError> {
    let map = PipeMap::new(input);

    debug!("{}", map.print_path(&vec![]));

    let start = map.start().expect("No start found");

    let possible_paths_from_start = map.possible_next_nodes(&start);
    debug!(
        "Possible Paths from: {:?}: {:?}",
        start, possible_paths_from_start
    );

    let mut possible_loops = vec![];
    for ele in possible_paths_from_start {
        debug!("Start: {:?}, found path to: {:?}", start, ele);
        let mut visited = vec![start];

        let mut optional = Some(ele);
        while let Some(next) = optional {
            visited.push(next);

            let newoptional: Vec<MapPosition> =
                map.possible_next_nodes(&next).into_iter().collect();
            optional = newoptional.into_iter().find(|x| !visited.contains(&x));
            //debug!("{}", map.print_path_centered(&visited, &next));
        }
        debug!("{}", map.print_path(&visited));
        possible_loops.push(visited);
    }

    possible_loops.sort_by_key(|v| v.len());
    let longest_loop = possible_loops.last().expect("No Loops Found!");
    //info!("Possible Loop: {:?}", possible_loops);
    let res: usize = (longest_loop.len() + 1) / 2;
    Ok(res.to_string())
}
enum RelativePosition {
    Left,
    Right,
    Up,
    Down,
}

struct PipeMap {
    grid: Array2<MapElement>,
}

fn to_array(input: &str) -> Array2<MapElement> {
    let chars: Vec<MapElement> = input
        .lines()
        .flat_map(|line| line.chars().filter(|&c| !c.is_whitespace()))
        .map(|c| MapElement::from(c))
        .collect();
    let rows = input.lines().count();
    let cols = input.lines().next().unwrap().chars().count();
    Array2::from_shape_vec((rows, cols), chars).unwrap()
}

impl PipeMap {
    pub fn new(input: &str) -> Self {
        Self {
            grid: to_array(input),
        }
    }

    fn print(&self) -> String {
        let mut s = String::from("\n");
        let mut current_row = 0;
        self.grid_indexed_iter().for_each(|e| {
            let index = e.0;
            if current_row != index.1 {
                s.push_str("\n");
                current_row = current_row + 1
            }
            s.push_str(&format!("{:?}: ", index));
            s.push_str(&format!("{:?} ", e.1.to_string()));
        });
        s
    }

    pub fn get(&self, index: (usize, usize)) -> Option<(MapPosition)> {
        self.grid.get((index.1, index.0)).map(|me| MapPosition {
            element: *me,
            index: index,
        })
    }

    pub fn start(&self) -> Option<(MapPosition)> {
        self.grid_indexed_iter()
            .find(|(_, ele)| *ele == &MapElement::Start)
            .map(|(i, e)| MapPosition {
                index: i,
                element: *e,
            })
    }
    pub fn print_path(&self, path: &Vec<MapPosition>) -> String {
        let paths: Vec<(usize, usize)> = path.iter().map(|p| (p.index.1, p.index.0)).collect();

        let mut s = String::from("\n");
        let mut current_row = 0;
        self.grid_indexed_iter().for_each(|e| {
            let index = e.0;
            if current_row != index.1 {
                s.push_str("\n");
                current_row = current_row + 1
            }
            if !paths.contains(&e.0) {
                s.push_str("");
                s.push_str(&e.1.to_string());
                s.push_str("");
            } else {
                s.push_str("#");
            }
        });
        s
    }

    fn find_viewport(&self, viewport: &MapPosition) -> ((usize, usize), (usize, usize)) {
        let center = viewport.index;

        let p1 = (center.0 as isize - 30, center.1 as isize - 30);
        let p2 = (center.0 as isize + 30, center.1 as isize + 30);

        let p1 = (p1.0.try_into().unwrap_or(0), p1.1.try_into().unwrap_or(0));
        let p2 = (p2.0.try_into().unwrap_or(0), p2.1.try_into().unwrap_or(0));
        (p1, p2)
    }

    fn grid_indexed_iter(&self) -> impl Iterator<Item = ((usize, usize), &MapElement)> {
        self.grid.indexed_iter().map(|(i, e)| ((i.1, i.0), e))
    }
    pub fn print_path_centered(&self, path: &Vec<MapPosition>, viewport: &MapPosition) -> String {
        let (p1, p2) = self.find_viewport(viewport);
        let paths: Vec<(usize, usize)> = path.iter().map(|p| p.index).collect();

        debug!("Viewport: {:?}", (p1, p2));

        let mut s = String::from("\n");
        let mut current_row = p1.1;
        self.grid_indexed_iter()
            .filter(|p| {
                //let p = p.0;
                let p = p.0;
                p.0 >= p1.0 && p.1 >= p1.1 && p.0 <= p2.0 && p.1 <= p2.1
            })
            .for_each(|e| {
                let index = e.0;
                if current_row != index.1 {
                    s.push_str("\n");
                    current_row = current_row + 1
                }
                if !paths.contains(&e.0) {
                    s.push_str("");
                    s.push_str(&e.1.to_string());
                    s.push_str("");
                } else {
                    s.push_str("#");
                }
            });
        s
    }
    pub fn possible_next_nodes(&self, position: &MapPosition) -> Vec<MapPosition> {
        let left: (isize, isize) = (-1, 0);
        let right: (isize, isize) = (1, 0);
        let up: (isize, isize) = (0, -1);
        let down: (isize, isize) = (0, 1);

        let possible_values = vec![left, right, up, down];
        let next_possible_way_point = possible_values
            .iter()
            .map(|(x_offset, y_offset)| {
                let index = position.index;
                (index.0 as isize + x_offset, index.1 as isize + y_offset)
            })
            //.inspect(|x| debug!("Possible neighbour: {:?}", x))
            .flat_map(|(x, y)| match (usize::try_from(x), usize::try_from(y)) {
                (Ok(x), Ok(y)) => Some((x, y)),
                (_, _) => None,
            })
            //.inspect(|x| debug!("Possible neighbour: {:?}", x))
            .flat_map(|index| self.get(index))
            /* .inspect(|x| {
                debug!(
                    "Possible Connection: {:?} - {:?}",
                    x.index,
                    x.element.to_string()
                )
            })*/
            .filter(|pos| position.is_connected(pos))
            /* .inspect(|x| {
                debug!(
                    "Connection Found: {:?} - {:?}",
                    x.index,
                    x.element.to_string()
                )
            })*/
            .collect();

        next_possible_way_point
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct MapPosition {
    element: MapElement,
    index: (usize, usize),
}

impl MapPosition {
    fn is_connected(&self, other: &MapPosition) -> bool {
        let relative_position = self.relative_position(other);

        let possible_connections = match (self.element, relative_position) {
            (MapElement::Vertical, RelativePosition::Left) => {
                vec![]
            }
            (MapElement::Vertical, RelativePosition::Right) => {
                vec![]
            }
            (MapElement::Vertical, RelativePosition::Up) => vec![
                MapElement::Vertical,
                MapElement::CornerSE,
                MapElement::CornerSW,
                MapElement::Start,
            ],
            (MapElement::Vertical, RelativePosition::Down) => vec![
                MapElement::Vertical,
                MapElement::CornerNE,
                MapElement::CornerNW,
                MapElement::Start,
            ],
            (MapElement::Horizonal, RelativePosition::Left) => {
                vec![
                    MapElement::Horizonal,
                    MapElement::CornerSE,
                    MapElement::CornerNE,
                    MapElement::Start,
                ]
            }
            (MapElement::Horizonal, RelativePosition::Right) => {
                vec![
                    MapElement::Horizonal,
                    MapElement::CornerSW,
                    MapElement::CornerNW,
                    MapElement::Start,
                ]
            }
            (MapElement::Horizonal, RelativePosition::Up) => vec![],
            (MapElement::Horizonal, RelativePosition::Down) => vec![],
            (MapElement::CornerNE, RelativePosition::Left) => vec![],
            (MapElement::CornerNE, RelativePosition::Up) => vec![
                MapElement::Vertical,
                MapElement::CornerSE,
                MapElement::CornerSW,
                MapElement::Start,
            ],

            (MapElement::CornerNE, RelativePosition::Right) => vec![
                MapElement::Horizonal,
                MapElement::CornerNW,
                MapElement::CornerSW,
                MapElement::Start,
            ],
            (MapElement::CornerNE, RelativePosition::Down) => vec![],

            (MapElement::CornerNW, RelativePosition::Left) => vec![
                MapElement::Horizonal,
                MapElement::CornerSE,
                MapElement::CornerNE,
                MapElement::Start,
            ],
            (MapElement::CornerNW, RelativePosition::Right) => vec![],
            (MapElement::CornerNW, RelativePosition::Up) => vec![
                MapElement::Vertical,
                MapElement::CornerSE,
                MapElement::CornerSW,
                MapElement::Start,
            ],
            (MapElement::CornerNW, RelativePosition::Down) => vec![],
            (MapElement::CornerSW, RelativePosition::Left) => vec![
                MapElement::Horizonal,
                MapElement::CornerNE,
                MapElement::CornerSE,
                MapElement::Start,
            ],
            (MapElement::CornerSW, RelativePosition::Right) => vec![],
            (MapElement::CornerSW, RelativePosition::Up) => vec![],
            (MapElement::CornerSW, RelativePosition::Down) => vec![
                MapElement::Vertical,
                MapElement::CornerNE,
                MapElement::CornerNW,
                MapElement::Start,
            ],
            (MapElement::CornerSE, RelativePosition::Left) => vec![],
            (MapElement::CornerSE, RelativePosition::Right) => vec![
                MapElement::Horizonal,
                MapElement::CornerSW,
                MapElement::CornerNW,
                MapElement::Start,
            ],
            (MapElement::CornerSE, RelativePosition::Up) => vec![],
            (MapElement::CornerSE, RelativePosition::Down) => vec![
                MapElement::Vertical,
                MapElement::CornerNE,
                MapElement::CornerNW,
                MapElement::Start,
            ],
            (MapElement::Ground, RelativePosition::Left) => vec![],
            (MapElement::Ground, RelativePosition::Right) => vec![],
            (MapElement::Ground, RelativePosition::Up) => vec![],
            (MapElement::Ground, RelativePosition::Down) => vec![],
            (MapElement::Start, RelativePosition::Left) => vec![
                MapElement::Horizonal,
                MapElement::CornerSE,
                MapElement::CornerNE,
            ],
            (MapElement::Start, RelativePosition::Right) => vec![
                MapElement::Horizonal,
                MapElement::CornerSW,
                MapElement::CornerNW,
            ],
            (MapElement::Start, RelativePosition::Up) => vec![
                MapElement::Vertical,
                MapElement::CornerSE,
                MapElement::CornerSW,
            ],
            (MapElement::Start, RelativePosition::Down) => vec![
                MapElement::Vertical,
                MapElement::CornerNE,
                MapElement::CornerNW,
            ],
        };
        possible_connections.contains(&other.element)
    }
    fn relative_position(&self, other: &MapPosition) -> RelativePosition {
        let (x, y) = self.index;
        let (other_x, other_y) = other.index;
        if x == other_x {
            if y > other_y {
                RelativePosition::Up
            } else {
                RelativePosition::Down
            }
        } else if y == other_y {
            if x > other_x {
                RelativePosition::Left
            } else {
                RelativePosition::Right
            }
        } else {
            panic!("Not connected")
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum MapElement {
    Vertical,
    Horizonal,
    CornerNE,
    CornerNW,
    CornerSW,
    CornerSE,
    Ground,
    Start,
}

impl From<char> for MapElement {
    fn from(c: char) -> Self {
        match c {
            '|' => MapElement::Vertical,
            '-' => MapElement::Horizonal,
            'L' => MapElement::CornerNE,
            'J' => MapElement::CornerNW,
            '7' => MapElement::CornerSW,
            'F' => MapElement::CornerSE,
            '.' => MapElement::Ground,
            'S' => MapElement::Start,
            _ => panic!("Invalid character for MapElement: {}", c),
        }
    }
}

impl fmt::Display for MapElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MapElement::Vertical => write!(f, "|"),
            MapElement::Horizonal => write!(f, "-"),
            MapElement::CornerNE => write!(f, "L"),
            MapElement::CornerNW => write!(f, "J"),
            MapElement::CornerSW => write!(f, "7"),
            MapElement::CornerSE => write!(f, "F"),
            MapElement::Ground => write!(f, "."),
            MapElement::Start => write!(f, "S"),
        }
    }
}

#[cfg(test)]
mod tests {
    use ndarray::arr2;
    use tracing::info;

    use super::*;
    use MapElement::*;

    #[test_log::test(test)]
    fn test_process() -> miette::Result<()> {
        let input = "7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ";
        assert_eq!("8", process(input)?);
        Ok(())
    }
    #[test_log::test(test)]
    fn test_process1() -> miette::Result<()> {
        let input = ".....
.S-7.
.|.|.
.L-J.
.....";
        assert_eq!("4", process(input)?);
        Ok(())
    }

    #[test_log::test(test)]
    fn test_to_map() -> miette::Result<()> {
        let expected = arr2(&[
            [CornerSW, Horizonal, CornerSE, CornerSW, Horizonal],
            [Ground, CornerSE, CornerNW, Vertical, CornerSW],
            [Start, CornerNW, CornerNE, CornerNE, CornerSW],
            [Vertical, CornerSE, Horizonal, Horizonal, CornerNW],
            [CornerNE, CornerNW, Ground, CornerNE, CornerNW],
        ]);
        let input = "7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ";

        let map = PipeMap::new(input);
        info!("{}", map.print());

        let expected_start = Some(MapPosition {
            element: MapElement::Start,
            index: (0, 2),
        });

        let expected_connections: Vec<MapPosition> = vec![
            MapPosition {
                element: MapElement::CornerNW,
                index: (1, 2),
            },
            MapPosition {
                element: MapElement::Vertical,
                index: (0, 3),
            },
        ];

        assert_eq!(expected, &map.grid);
        assert_eq!(expected_start, map.start());
        assert_eq!(
            Some(MapPosition {
                element: CornerSW,
                index: (0, 0)
            }),
            map.get((0, 0))
        );

        assert_eq!(
            Some(MapPosition {
                element: Ground,
                index: (2, 4)
            }),
            map.get((2, 4))
        );

        assert_eq!(
            expected_connections,
            map.possible_next_nodes(&expected_start.unwrap())
        );

        assert_eq!(
            vec![
                MapPosition {
                    element: CornerSW,
                    index: (4, 2)
                },
                MapPosition {
                    element: Vertical,
                    index: (3, 1)
                }
            ],
            map.possible_next_nodes(&map.get((3, 2)).unwrap())
        );

        Ok(())
    }
}
